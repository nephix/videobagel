import criticalCssPlugin from 'preact-cli-plugin-critical-css';

export default (config, env) => {
	const options = {
		// Passed directly to the 'critical' module (this is optional)
	};

	if (env.isProd) {
		config.devtool = false; // disable sourcemaps
	}

	criticalCssPlugin(config, env, options);
};
