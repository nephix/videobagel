import { h } from 'preact';
import { Text } from 'preact-i18n';

import Checkmark from '../../components/checkmark';
import style from './style';

const OrderConfirmation = () => (
	<div class="container">
		<Checkmark color="#5bd8a6" />
		<h1>
			<Text id="questionnaire.headline">Thank You! Your order was successfully placed.</Text>
		</h1>
		<p class={style.stepText}>
			<Text id="confirmation.contactYou">We will contact you within 1 business day.</Text>
		</p>
	</div>
);

export default OrderConfirmation;
