import { h } from 'preact';
import style from './style';

const Legal = () => (
	<div class="container">
		<h1>Legal</h1>
		<p> Information provided according to Sec. 5 German Telemedia Act (TMG) </p>
		<p> Thomas Pischulski</p>
		<p> Video Bagel </p>
		<p> Straße der Republik 24 </p>
		<p> 15890 Eisenhüttenstadt </p>
		<p> Germany </p>
		
		<h2 class={style.headline}>Contact</h2>
		<p> E-Mail: support@videobagel.com </p>
		<p> Telephone: +1 786 600 1245 </p>
		<p> Website: https://www.videobagel.com </p>
		
		<h2 class={style.headline}>VAT number</h2>
		<p> VAT indentification number in accordance with section 27 a of the German VAT act DE313794194 </p>
		<p> We do not take part in online dispute resolutions at consumer arbitration boards. </p>
		
		<h2 class={style.headline}>Liability for Contents</h2>
		<p> As service providers, we are liable for own contents of these websites according to Sec. 7, paragraph 1 German Telemedia Act (TMG). However, according to Sec. 8 to 10 German Telemedia Act (TMG), service providers are not obligated to permanently monitor submitted or stored information or to search for evidences that indicate illegal activities.
		Legal obligations to removing information or to blocking the use of information remain unchallenged. In this case, liability is only possible at the time of knowledge about a specific violation of law. Illegal contents will be removed immediately at the time we get knowledge of them. </p>
		
		<h2 class={style.headline}>Liability for Links</h2>
		<p> Our offer includes links to external third party websites. We have no influence on the contents of those websites, therefore we cannot guarantee for those contents. Providers or administrators of linked websites are always responsible for their own contents. </p>
		<p> The linked websites had been checked for possible violations of law at the time of the establishment of the link. Illegal contents were not detected at the time of the linking. A permanent monitoring of the contents of linked websites cannot be imposed without reasonable indications that there has been a violation of law. Illegal links will be removed immediately at the time we get knowledge of them. </p>

		<h2 class={style.headline}>Copyright</h2>
		<p> Contents and compilations published on these websites by the providers are subject to German copyright laws. Reproduction, editing, distribution as well as the use of any kind outside the scope of the copyright law require a written permission of the author or originator. Downloads and copies of these websites are permitted for private use only.
		The commercial use of our contents without permission of the originator is prohibited.
		Copyright laws of third parties are respected as long as the contents on these websites do not originate from the provider. Contributions of third parties on this site are indicated as such. However, if you notice any violations of copyright law, please inform us. Such contents will be removed immediately. </p>
	</div>
);

export default Legal;
