import { h } from 'preact';
import style from './style';

const CopywriterJob = () => (
	<div class="container">
		<h1>Copywriter</h1>
		<div class={style.pin}></div>
		<div class={style.pulse}></div>
		<p class={style.location}>Remote working opportunity</p>
		<p>The Video Bagel team is looking for a Copywriter who excels in producing creative communication and coming up with big ideas. The ideal candidate understands the dynamics involved communicating an idea through words and video in an efficient way.</p>
		<h2 class={style.headline}>Responsibilities</h2>
		<ul class={style.requirementList}>
			<li>Come up with creative video plot ideas</li>
			<li>Write persuasive video scripts that will trigger an action from the viewer</li>
			<li>Get the client’s point across in a funny and entertaining way</li>
		</ul>
		<h2 class={style.headline}>What We Offer</h2>
		<ul  class={style.requirementList}>
			<li>Work whatever hours you like</li>
			<li>Work from anywhere you want</li>
		</ul>
		<h2 class={style.headline}>Qualifications</h2>
		<ul  class={style.requirementList}>
			<li>Min. 1 years of direct-response copywriting experience in one of the following digital mediums: Websites, apps, social channels, emails, or video</li>
			<li>Exceptional and flexible writing and editing skills</li>
			<li>Ability to work with a diverse range of clients</li>
			<li>Familiar with software like Google Drive or MS Office</li>
		</ul>
		<p>If you want to apply for this job write a friendly email to jobs@videobagel.com.</p>
	</div>
);

export default CopywriterJob;
