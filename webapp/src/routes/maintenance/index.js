import { h } from 'preact';

const Maintenance = () => (
	<div class="container">
		<h1>Sorry, we're currently under maintenance!</h1>
	</div>
);

export default Maintenance;