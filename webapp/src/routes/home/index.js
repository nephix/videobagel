import { h, Component } from 'preact';
import { Text, MarkupText, withText } from 'preact-i18n';
import classNames from 'classnames/bind';

import style from './style';

import Testimonial from '../../components/testimonial';
import PriceRangeSlider from '../../components/priceRangeSlider';
import MadeInIcons from '../../components/madeinicons';
import OrderForm from '../../components/orderForm';
//import PaymentForm from '../../components/paymentForm';
import DemoVideo from '../../components/demoVideo';
import MainVideo from '../../components/mainVideo';
import Team from '../../components/team';

let cx = classNames.bind(style);

@withText({
	processVideoThumbnail: <Text id="video.process">assets/process.svg</Text>,
	showreelThumbnail: <Text id="video.showreel">assets/watch_reel.svg</Text>,
	advertisingThumbnail: <Text id="video.advertising">/assets/adv_strategy.svg</Text>,
	contentThumbnail: <Text id="video.content">/assets/content_planning.svg</Text>,
	salesThumbnail: <Text id="video.sales">/assets/sales_video.svg</Text>,
	customGraphicsTooltip: (
		<Text id="homepage.order.customGraphicsTooltip">
			Custom illustrations are billed separately at 99 &euro; per graphic due to the extra resources
			required.
		</Text>
	),
	email: <Text id="footer.emailAddress">bWFpbHRvOmhlbGxvQHZpZGVvYmFnZWwuY29t</Text>
})
export default class Home extends Component {
	onClickCTA() {
		document
			.querySelector('#pricing h2')
			.scrollIntoView({ block: 'start', inline: 'nearest', behavior: 'smooth' });
	}

	scrollToCheckout() {
		document
			.querySelector('#order')
			.scrollIntoView({ block: 'start', inline: 'nearest', behavior: 'smooth' });
	}

	onEmailClicked = () => {
		// track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/d3moCKfwtnoQrumnjAM',
				transaction_id: ''
			});
		}

		// Track mixpanel conversion
		if (window.mixpanel) {
			mixpanel.track('Clicked Contact Email');
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Lead');
		}
	};

	componentDidMount() {
		const { email } = this.props;
		this.setState({ email: atob(email) });
	}

	constructor(props) {
		super(props);
		this.state = { email: null };
	}

	render({
		processVideoThumbnail,
		showreelThumbnail,
		customGraphicsTooltip,
		advertisingThumbnail,
		salesThumbnail,
		contentThumbnail
	}) {
		const { email } = this.state;
		return (
			<div>
				<section id="usp">
					<div class="container">
						<MarkupText id="homepage.title">
							<h1>
								Hey, do you want to <em>explain</em> your product or automate the delivery of your{' '}
								<em>optimal sales message</em> to grow your business? 👇
							</h1>
						</MarkupText>

						<MainVideo
							vimeoUrl="https://player.vimeo.com/video/269488214"
							thumb={showreelThumbnail}
							alt="Showreel"
						/>

						<div class={style.buttonWrapper}>
							<a class={style.btn} onClick={() => this.onClickCTA()}>
								<Text id="homepage.mainCallToAction">Yes, let's get started!</Text>
							</a>
						</div>
						<MadeInIcons />
					</div>
				</section>
				<section id="reviews">
					<div class="container">
						<MarkupText id="homepage.valueProposition">
							<h2>
								We make multilingual <em>animated videos</em> for:
							</h2>
						</MarkupText>
						<div class={style.appScreen}>
							<ul>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/webapp.svg" alt="Web Applications" />
									</span>
									<span class={style.appName}>
										Mobile/
										<br />
										Web Apps
									</span>
								</li>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/vacuum.svg" alt="Innovative Products" />
									</span>
									<span class={style.appName}>
										<Text id="general.products">Products</Text>
									</span>
								</li>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/services.svg" alt="Professional Services" />
									</span>
									<span class={style.appName}>
										<Text id="general.services">Services</Text>
									</span>
								</li>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/fb.svg" alt="Facebook" />
									</span>
									<span class={style.appName}>Facebook</span>
								</li>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/instagram.svg" alt="Instagram" />
									</span>
									<span class={style.appName}>Instagram</span>
								</li>
								<li>
									<span class={style.appIcon}>
										<img src="/assets/youtube.svg" alt="YouTube" />
									</span>
									<span class={style.appName}>YouTube</span>
								</li>
							</ul>
						</div>
						<div class={style.testimonialSection}>
							<Testimonial
								photo="/assets/testimonials/53895231.jpg"
								name="Dean P."
								date="Nov 1, 2017"
								text="VideoBagel is magnificent! Thomas took our plain text, and created a high-quality video from it. His storyboards were useful in the drafting process. His actress nailed our lines too. Highly recommended."
							/>
							<Testimonial
								photo="/assets/testimonials/34289573.jpg"
								name="Andrew C."
								date="May 3, 2017"
								text="Good work on my video. Thanks."
							/>
							<Testimonial
								photo="/assets/testimonials/23622232.jpg"
								name="Kristin C."
								date="Nov 27, 2017"
								text="I would absolutely recommend VideoBagel! They were very speedy in getting our video done, were very prompt in answering any questions/concerns we had along the way. They listened to us, took our information, and produced a high-quality video that we were very pleased with. I particularly liked how we were consulted along the way. Thomas sent us the draft of the script and storyboards for our approval- before completing the finished product. It was nice to have a professional make the video for us, but I really liked how we were able to have input along the way."
							/>
							<Testimonial
								photo="/assets/testimonials/17353470.jpg"
								name="Vincent K."
								date="Apr 18, 2018"
								text="I would definitely recommend Video Bagel. Thomas and his team are professional, timely on deliverables, and very easy to work with. They listened to our needs and produced a high quality video. Some video production companies allow only one or two rounds of feedback. But with Video Bagel they we were able to provide feedback and input in every step of the process until our requirements were met. I look forward to working with Video Bagel again on future projects."
							/>

							<Testimonial
								photo="/assets/testimonials/34572367.jpg"
								name="Brett P."
								date="Feb 21, 2018"
								text="If you want more money in your pocket then you should be running video ads. We use Thomas and his team to create our videos because they are quick, efficient, and most importantly, know what is going to convert. Thanks guys, we’ll be back soon for more awesome videos."
							/>
							<Testimonial
								photo="/assets/testimonials/11043534.jpg"
								name="Abdellah O."
								date="May 7, 2018"
								text="Je recommande vivement Video Bagel. Sérieusement, les gens sont réactifs, flexibles, super agréable, et surtout extrêmement compétent. J'ai jamais rencontré de service aussi nickel qu'eux. Thomas est ultra disponible, bienveillant et surtout créatif. Cette équipe réalise des merveilles en un temps record pour 4 à 10 fois moins cher que la concurrence. je leur souhaite beaucoup de succès, en tout cas il le mérite. Abdellah, Sendeezy."
							/>
							<Testimonial
								photo="/assets/testimonials/29871675.jpg"
								name="Davide V."
								date="May 8, 2018"
								text="Video Bagel guys have been really flexible, professional and quick in delivering the video. I dealt with Thomas and I think he is a good person first of all but surely capable and knowledgable in his job. I highly recommend these guys."
							/>
							<Testimonial
								photo="/assets/testimonials/34647318.jpg"
								name="Cassie C."
								date="June 19, 2018"
								text="Thomas and his team at Video Bagel were fantastic to work with. They listened to our brief and delivered way past our expectations! We would highly recommend Video Bagel for anyone wanted to use a video platform for their business. Great work guys!"
							/>
							<Testimonial
								photo="/assets/testimonials/67737215.jpg"
								name="Brian B."
								date="January 3, 2018"
								text="Video Bagel just finished our second video today and we couldn't be more satisfied. They helped us come up with a script that focused on our product's benefits in a fun and interesting way. Within seven days we had a completed 60 second video. Our first video had over 5000 views in the first month and has helped us reach a few solid leads. If you want a solid, engaging video, you can't do much better than Video Bagel."
							/>
							<Testimonial
								photo="/assets/testimonials/nophoto.jpg"
								name="Jason C."
								date="March 22, 2018"
								text="They did a great job of taking my concept and making a professional pitch video. I never worked with a pitch video company before and the experience working with them was good. They made sure I approved of each step throughout the process and wanted my feedback. As a result the animation and overall video quality was world class. I would recommend them to others who want a company to make a pitch video for them."
							/>
							<Testimonial
								photo="/assets/testimonials/22228133.jpg"
								name="Keith C."
								date="Oct 19, 2018"
								text="Just want to thank Thomas and his team for the hard work they put in! Great job in making our cool videos and great customer service!! Highly recommended!"
							/>
						</div>
						<div class={style.buttonWrapper}>
							<a
								className={cx({ btn: true, ghost: true })}
								href="https://facebook.com/videobagel/reviews"
								rel="noopener noreferrer"
								target="_blank"
							>
								<Text id="homepage.moreReviews">See more reviews on Facebook</Text>
							</a>
						</div>
					</div>
				</section>
				<section id="benefits">
					<div class="container">
						<h2 class={style.quote}>
							<MarkupText id="homepage.linkedInQuote">
								“We know that viewers, when they watch video, the message is{' '}
								<em>much more likely received</em> when compared to just text.”
							</MarkupText>
						</h2>

						<div class={style.quoteName}>Sudeep Cherian</div>
						<div class={style.position}>
							<Text id="homepage.linkedInPosition">Head of Product Marketing at</Text>
						</div>
						<div class={style.company}>
							<img src="/assets/linkedin.png" alt="LinkedIn" />
						</div>
						<ul class={style.benefits}>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.timeOnPage.headline">
										Video Content Drastically <br />
										Increases Time on Page
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.timeOnPage.text">
										“People spent on average 2.6x more time on pages with video than without” -
										Wistia, according to their media player pixel usage.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.higherCTRs.headline">
										Video Ads Have <br />
										Higher Link CTRs
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.higherCTRs.text">
										“Video ads have an average click-through rate of 1.84%, the highest
										click-through rate of all digital ad formats” - Business Insider, BI
										Intelligence report.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.seo.headline">
										Improve
										<br />
										SEO Rankings
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.seo.text">
										Kissmetrics suggests to provide more information in form of an explainer video
										on pages with high bounce rates. Through the video, people will spend more time
										on your site &mdash; which the search engines love.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.reduceChurnRate.headline">
										Video Tutorials <br />
										Reduce Churn Rates
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.reduceChurnRate.text">
										“Customers that show behaviors that indicate they are not on a path to success
										need attention.” Evergage suggests that a specific video tutorial can help to
										reduce membership cancellations.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.websiteConversions.headline">
										Sales Videos Increase <br />
										Website Conversions
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.websiteConversions.text">
										CrazyEgg says that “people who watch a video of your product are as much as 85%
										more likely to purchase.” What would that mean to your bottom line?
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.crowdfundingOdds.headline">
										Explainer Videos Raise <br /> Crowdfunding Campaign Odds
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.crowdfundingOdds.text">
										Those crowdfunding campaigns that use pitch videos are 75% more likely to
										achieve their goal than those with no video at all.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.longTermMemory.headline">
										Explainer Videos
										<br /> Stick in Long-Term Memory
									</MarkupText>
								</h2>
								<p>
									<Text id="homepage.benefits.longTermMemory.text">
										According to Forbes, simple animated videos provide a message format best-suited
										for long-term memory retention. The simple, cartoonish nature also holds the
										viewers' attention substantially longer than other type of videos.
									</Text>
								</p>
							</li>
							<li>
								<h2>
									<MarkupText id="homepage.benefits.growRevenue.headline">
										Videos Grow
										<br /> Revenue 49% Faster
									</MarkupText>
								</h2>
								<p>
									<MarkupText id="homepage.benefits.growRevenue.text">
										Companies that utilize videos with their product or marketing grow on average
										49% faster on a year-to-year basis than those who don't, according to{' '}
										<a href="https://blog.hubspot.com/marketing/top-video-marketing-statistics">
											HubSpot
										</a>
										.
									</MarkupText>
								</p>
							</li>
						</ul>
					</div>
				</section>
				<section id="examples">
					<div class="container">
						<h2>
							<MarkupText id="homepage.ourWork">
								Here is some of <em>our work</em>
							</MarkupText>
						</h2>
						<div class={style.demoVideos}>
							<DemoVideo
								description="AVNow"
								thumb="assets/demo/demo_video_9.jpg"
								embed="https://player.vimeo.com/video/267083637"
								country="🇺🇸"
							/>
							<DemoVideo
								description="V-Comb"
								thumb="assets/demo/demo_video_19.jpg"
								embed="https://player.vimeo.com/video/290631746"
								country="🇦🇺"
							/>
							<DemoVideo
								description="V-Comb"
								thumb="assets/demo/demo_video_20.jpg"
								embed="https://player.vimeo.com/video/290631782"
								country="🇦🇺"
							/>
							<DemoVideo
								description="Murge"
								thumb="assets/demo/demo_video_16.jpg"
								embed="https://player.vimeo.com/video/268345897"
								country="🇬🇧"
							/>
							<DemoVideo
								description="PIA"
								thumb="assets/demo/demo_video_18.jpg"
								embed="https://player.vimeo.com/video/269603201"
								country="🇦🇺"
							/>
							<DemoVideo
								description="SponsorCube Explainer Video"
								thumb="assets/demo/demo_video_15.jpg"
								embed="https://player.vimeo.com/video/267370286"
								country="🇭🇰"
							/>
							<DemoVideo
								description="SponsorCube Promo Video"
								thumb="assets/demo/demo_video_26.jpg"
								embed="https://player.vimeo.com/video/308658902"
								country="🇭🇰"
							/>
							<DemoVideo
								description="SponsorCube Marbury"
								thumb="assets/demo/demo_video_27.jpg"
								embed="https://player.vimeo.com/video/308658683"
								country="🇭🇰"
							/>
							<DemoVideo
								description="Dovetail"
								thumb="assets/demo/demo_video_21.jpg"
								embed="https://player.vimeo.com/video/286913420"
								country="🇦🇺"
							/>
							<DemoVideo
								description="DrPrint"
								thumb="assets/demo/demo_video_22.jpg"
								embed="https://player.vimeo.com/video/307019441"
								country="🇦🇺"
							/>
							<DemoVideo
								description="Sendeezy"
								thumb="assets/demo/demo_video_17.jpg"
								embed="https://player.vimeo.com/video/308757215"
								country="🇫🇷"
							/>
							<DemoVideo
								description="CheckThem"
								thumb="assets/demo/demo_video_4.jpg"
								embed="https://player.vimeo.com/video/267083239"
								country="🇺🇸"
							/>
							<DemoVideo
								description="enCloud9 Sales Accelerator"
								thumb="assets/demo/demo_video_7.jpg"
								embed="https://player.vimeo.com/video/267083475"
								country="🇺🇸"
							/>
							<DemoVideo
								description="enCloud9 Marketing Accelerator"
								thumb="assets/demo/demo_video_10.jpg"
								embed="https://player.vimeo.com/video/267083559"
								country="🇺🇸"
							/>
							<DemoVideo
								description="Dr Kiseki"
								thumb="assets/demo/demo_video_25.jpg"
								embed="https://player.vimeo.com/video/286913704"
								country="🇲🇾"
							/>
							<DemoVideo
								description="Bucketio"
								thumb="assets/demo/demo_video_8.jpg"
								embed="https://player.vimeo.com/video/267082653"
								country="🇺🇸"
							/>
							<DemoVideo
								description="Simplby"
								thumb="assets/demo/demo_video_12.jpg"
								embed="https://player.vimeo.com/video/267083676"
								country="🇰🇷🇨🇳"
							/>
							<DemoVideo
								description="EMB Malaysia"
								thumb="assets/demo/demo_video_14.jpg"
								embed="https://player.vimeo.com/video/267084251"
								country="🇬🇧"
							/>
						</div>
					</div>
				</section>
				<section id="content">
					<div class="container">
						<h2>
							<MarkupText id="homepage.content.title">
								Need some help to <em>get started</em>?
							</MarkupText>
						</h2>

						<div class={style.contentVideos}>
							<DemoVideo
								description="Advertising"
								thumb={advertisingThumbnail}
								embed="https://player.vimeo.com/video/310538908"
								square="true"
								grayscale="false"
							/>
							<DemoVideo
								description="Content"
								thumb={contentThumbnail}
								embed="https://player.vimeo.com/video/310539098"
								square="true"
								grayscale="false"
							/>
							<DemoVideo
								description="Sales"
								thumb={salesThumbnail}
								embed="https://player.vimeo.com/video/310538998"
								square="true"
								grayscale="false"
							/>
						</div>
					</div>
				</section>
				{/*
				<section id="how">
					<div class="container">
						<h2>
							<em>
								<Text id="homepage.howItWorks">How's our process?</Text>
							</em>
						</h2>
						<ol class={style.process}>
							<li>
								<h3>
									<MarkupText id="homepage.process.yourBusiness.headline">
										Your Business &amp;
										<br /> Customers
									</MarkupText>
								</h3>
								<p>
									<MarkupText id="homepage.process.yourBusiness.text">
										After submitting your order, we will get in touch with you to learn more about
										your business and your customers. This will help us to better understand what
										you're after: Getting more sign ups, more appointments, more leads, more sales,
										or getting more visibility, less subscription cancellations or less customer
										service enquiries.
									</MarkupText>
								</p>
							</li>
							<li>
								<h3>
									<MarkupText id="homepage.process.story.headline">Your Unique Story</MarkupText>
								</h3>
								<p>
									<Text id="homepage.process.story.text">
										The foundation of a successful video is always a solid script. After we've
										identified your customers, our copywriters will start to write a script. We want
										your video to directly hook into your future customers’ on-going inner dialog.
										Next, we present your product as a viable solution that can help them to
										overcome their problems.
									</Text>
								</p>
							</li>
							<li>
								<h3>
									<Text id="homepage.process.voice.headline">Voice Talent</Text>
								</h3>
								<p>
									<Text id="homepage.process.voice.text">
										Nobody wants their message to be delivered by a monotone robotic voice in a
										weird accent. We'll hire a professional vocal artist for your video. The right
										voice can create immediate chemistry between the viewer and your product.
									</Text>
								</p>
							</li>
							<li>
								<h3>
									<Text id="homepage.process.concept.headline">Visual Concept</Text>
								</h3>
								<p>
									<Text id="homepage.process.concept.text">
										Next, we'll take the script and create a visual concept for your video. We'll
										design engaging scenes that smoothly deliver your message. Together, we'll go
										through several drafts until you're completely satisfied.
									</Text>
								</p>
							</li>
							<li>
								<h3>
									<MarkupText id="homepage.process.sound.headline">
										Motion &amp; Sound
										<br /> Design
									</MarkupText>
								</h3>
								<p>
									<Text id="homepage.process.sound.text">
										It's time to put all the pieces together. We'll take everything we've done so
										far and produce your video. We'll add sound effects and custom made graphics
										too, if necessary. You can rest assured that you'll receive a high quality
										video.
									</Text>
								</p>
							</li>
							<li>
								<h3>
									<MarkupText id="homepage.process.delivery.headline">
										Delivering Your
										<br /> Video
									</MarkupText>
								</h3>
								<p>
									<Text id="homepage.process.delivery.text">
										Now that your video is finalized, it will be delivered to you promptly. We can
										also give you advice on how to get the video in front of your audience. Your ROI
										and campaign targets are important for us.
									</Text>
								</p>
							</li>
						</ol>
					</div>
				</section>
				*/}
				<section id="team">
					<Team />
				</section>
				<section id="faq">
					<div class="container">
						<h2>
							<em>
								<Text id="homepage.faq.headline">Have a question?</Text>
							</em>
						</h2>
						<ul class={style.faq}>
							<li class={style.tab}>
								<input id="gettingStarted" type="radio" name="tabs" />
								<label for="gettingStarted">
									<Text id="homepage.faq.gettingStarted.question">How do I get started?</Text>
								</label>
								<div class={style.tabContent}>
									<p>
										<Text id="homepage.faq.gettingStarted.answer">
											You can get started by ordering your video straight here on our website. After
											that, We will then go through all the information that you have provided and
											get in touch with you if we have additional questions. After that, we'll send
											you an invoice and get started to produce your video after you've made the
											payment.
										</Text>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="quotation" type="radio" name="tabs" />
								<label for="quotation">
									<Text id="homepage.faq.quotation.question">Can you send me a quote?</Text>
								</label>
								<div class={style.tabContent}>
									<p>
										<Text id="homepage.faq.quotation.answer">
											You can contact us through our email and ask us for an estimate. You will
											receive an invoice after you've placed your order with the form below.
										</Text>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="paymentMethods" type="radio" name="tabs" />
								<label for="paymentMethods">
									<Text id="homepage.faq.paymentMethods.question">
										Which payment methods do you accept?
									</Text>
								</label>
								<div class={style.tabContent}>
									<p>
										<Text id="homepage.faq.paymentMethods.answer">
											We accept credit cards and PayPal.
										</Text>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="story" type="radio" name="tabs" />
								<label for="story">
									<Text id="homepage.faq.story.question">Can you propose a plot before I buy?</Text>
								</label>
								<div class={style.tabContent}>
									<p>
										<Text id="homepage.faq.story.answer">
											We totally understand where your question is coming from, but we don't do
											something for nothing and encourage you to share our view. If, after your
											purchase, you feel like things are heading into a wrong direction you can
											always request a refund.
										</Text>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="payment" type="radio" name="tabs" />
								<label for="payment">
									<Text id="homepage.faq.payment.question">How does the payment work?</Text>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.payment.answer">
											After you've submitted the order form you will receive an invoice that include
											the whole amount for the project. In order to begin working on your project,
											you will need to pay the invoice. For existing customers, we offer the option
											to make a 50% down payment and pay the other half after the delivery of the
											video.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="deliveryTime" type="radio" name="tabs" />
								<label for="deliveryTime">
									<MarkupText id="homepage.faq.deliveryTime.question">
										How long does it take you to deliver?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.deliveryTime.answer">
											Our turnaround time for a video is about 5-10 work days. Depending on our
											current project load,{' '}
											<strong>your project might be queued for a couple of days initially</strong>.
											You have the option to jump ahead of the queue for an extra fee of 99 &euro;.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="refund" type="radio" name="tabs" />
								<label for="refund">
									<MarkupText id="homepage.faq.refund.question">
										What if I don't like the video?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.refund.answer">
											We have a 100% satisfaction guarantee. If you feel like we've wasted your time
											or money, you can request a full refund until the final delivery.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="howManyWords" type="radio" name="tabs" />
								<label for="howManyWords">
									<MarkupText id="homepage.faq.howManyWords.question">
										How many words fit into 30 seconds?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.howManyWords.answer">
											Our copywriters will write a script that contains around 75 words per 30
											seconds. This will allow people to understand the message while simultanously
											being able to follow the animation. The 75 word limit isn't hard.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="language" type="radio" name="tabs" />
								<label for="language">
									<MarkupText id="homepage.faq.language.question">
										Can you do any language or accent?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.language.answer">
											Yes, we can do videos in most languages and accents. We have already done
											videos in American English, British English, Australian English, Spanish,
											German, French and Chinese. If you order a language other than English, you
											will need to order the subtitle add on for 69 &euro; as well. The subtitles
											will allow us to match the timing each scene with the voice over and sentence,
											even if we don't speak the language.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="hiddenCosts" type="radio" name="tabs" />
								<label for="hiddenCosts">
									<MarkupText id="homepage.faq.hiddenCosts.question">
										Are there any hidden costs?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.hiddenCosts.answer">
											The prices on our website offer a complete package that includes everything
											from script writing, voice talent, visual concept creation and final
											animation. We offer some additional services in our project questionnaire.
											Most of our customers don't want these add ons, therefore they're optional.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="software" type="radio" name="tabs" />
								<label for="software">
									<MarkupText id="homepage.faq.software.question">
										What kind of software are you using?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.software.answer">
											For the script writing and storyboard phase we use Google Docs and Slides.
											Custom graphics and illustrations are created in Adobe Illustrator. We animate
											with Adobe After Effects.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="illustrations" type="radio" name="tabs" />
								<label for="illustrations">
									<MarkupText id="homepage.faq.illustrations.question">
										What is as a custom illustration?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.illustrations.answer">
											Custom illustrations are for example an animated version of your product, a
											very unique character or a very unique and detailled environment that you
											can't provide or that requires a lot of extra efforts on our side. We charge
											99 &euro; per custom graphic.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="customMade" type="radio" name="tabs" />
								<label for="customMade">
									<MarkupText id="homepage.faq.customMade.question">
										Are all of your videos custom made?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.customMade.answer">
											We don't use any video templates for our videos, unless you specifically
											require it. All our videos are custom made from script writing to the finished
											animation. We may use some template illustrations to ensure fast delivery
											times and to avoid charging you for custom illustrations.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="revisions" type="radio" name="tabs" />
								<label for="revisions">
									<MarkupText id="homepage.faq.revisions.question">
										How many revisions do I get?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.revisions.answer">
											You get <strong>unlimited revisions</strong>.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="script" type="radio" name="tabs" />
								<label for="script">
									<MarkupText id="homepage.faq.script.question">
										Can you use my script for the video?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.script.answer">
											Sure you can give input at any stage of the process. Our prices will remain
											the same though.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="dimensions" type="radio" name="tabs" />
								<label for="dimensions">
									<MarkupText id="homepage.faq.dimensions.question">
										I need specific dimensions.
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.dimensions.answer">
											No problem, we'll provide you with any dimension that you want. Keep in mind
											that in order to use your video for advertising, the dimensions need to comply
											to e.g. Facebook's or YouTube's advertising policy.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="3d" type="radio" name="tabs" />
								<label for="3d">
									<MarkupText id="homepage.faq.3d.question">Can you do 3D videos?</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.3d.answer">Nope, sorry.</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="actor" type="radio" name="tabs" />
								<label for="actor">
									<MarkupText id="homepage.faq.actor.question">
										Is an actor included in the price?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.actor.answer">
											No, it's an add on service and you will be asked about it in the
											questionnaire. An actor or actress will be 249 &euro; extra per 30 seconds of
											video.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="variations" type="radio" name="tabs" />
								<label for="variations">
									<MarkupText id="homepage.faq.variations.question">
										I want different variations.
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.variations.answer">
											We do variations at a reduced rate. Contact us and we can give you a quote.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="subtitles" type="radio" name="tabs" />
								<label for="subtitles">
									<MarkupText id="homepage.faq.subtitles.question">
										Can you add subtitles?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.subtitles.answer">
											Yes, this is an add on service and you will be asked about it in the
											questionnaire. We offer subtitles as SRT file and hardcoded into the video for
											an additional 69 &euro;.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="communication" type="radio" name="tabs" />
								<label for="communication">
									<MarkupText id="homepage.faq.communication.question">
										How will we communicate?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.communication.answer">
											Initially we will contact you via email or phone. To kick off the project, you
											will be invited into our Slack and work directly with our team.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="writing" type="radio" name="tabs" />
								<label for="writing">
									<MarkupText id="homepage.faq.writing.question">
										Who's writing the scripts?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.writing.answer">
											Your script will be written by our American copywriter Amanda. All scripts,
											even if the final video will be in a different language, are written in
											English first and are then translated by a native speaker into the target
											language.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="location" type="radio" name="tabs" />
								<label for="location">
									<MarkupText id="homepage.faq.location.question">
										Where are you located?
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.location.answer">
											We're a remote friendly company that is legally registered in Germany.
										</MarkupText>
									</p>
								</div>
							</li>

							<li class={style.tab}>
								<input id="contact" type="radio" name="tabs" />
								<label class={style.highlightLabel} for="contact">
									<MarkupText id="homepage.faq.contact.question">
										Can I talk to someone...
									</MarkupText>
								</label>
								<div class={style.tabContent}>
									<p>
										<MarkupText id="homepage.faq.contact.answer" fields={{ email }}>
											Sure, send us a friendly message through our{' '}
											<a href="/contact">contact form</a> or an{' '}
											<a href={email} onClick={() => this.onEmailClicked()}>
												email
											</a>{' '}
											and we'll answer any question you have.
										</MarkupText>
									</p>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<section id="pricing">
					<div class="container">
						<div class={style.offerWrapper}>
							<PriceRangeSlider />
							<h2 class={style.orderHeadline}>
								<Text id="homepage.order.include">All our videos include:</Text>
							</h2>
							<div class={style.offers}>
								<ul class={style.features}>
									<li>
										<Text id="homepage.order.copywriting">Copywriting</Text>
									</li>
									<li>
										<Text id="homepage.order.voiceOver">Studio Quality Voiceover</Text>
									</li>
									<li>
										<Text id="homepage.order.storyboard">Visual Concept & Storyboard</Text>
									</li>
									<li>2D Animation</li>
									<li>Full HD 1080p</li>
									<li>
										&infin; <Text id="homepage.order.revisions">Revisions</Text>
									</li>
									<li>
										<Text id="homepage.order.delivery">3-4 Week Delivery</Text>
									</li>
									<li>White Label</li>
									<li class={style.excluded}>
										<Text id="homepage.order.customGraphics">Custom Illustrations</Text>
										<i
											onClick={e => e.preventDefault()}
											class={style.tooltip}
											data-tooltip={customGraphicsTooltip}
										/>
									</li>
								</ul>
							</div>
							<div class={style.contactFormWrapper}>
								<div class={style.doubleArrowDown}>
									<img
										src="/assets/double-arrow-down.svg"
										alt="Order Now"
										onClick={() => this.scrollToCheckout()}
									/>
								</div>
								<h2 id="order">
									<MarkupText id="homepage.order.soundsGood">
										Sounds good? <br />
										<em>Order Your Video Below!</em>
									</MarkupText>
								</h2>
								<OrderForm />
								<div class={style.trustWrapper}>
									<ul class={style.trustIconList}>
										<li>
											<img src="/assets/padlock.svg" alt="Secure Connection" />
											<p>
												<MarkupText id="homepage.securedConnection">
													Secure <br />
													Payment
												</MarkupText>
											</p>
										</li>
										<li>
											<img src="/assets/letsencrypt.svg" alt="Encrypted Connection" />
											<p>
												<MarkupText id="homepage.encryptedConnection">
													Encrypted <br />
													Connection
												</MarkupText>
											</p>
										</li>
										<li>
											<img src="/assets/shield.svg" alt="GDPR Compliant" />
											<p>
												<MarkupText id="homepage.privacyProtection">
													GDPR <br />
													Compliance
												</MarkupText>
											</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}
