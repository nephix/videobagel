import { h } from 'preact';
import style from './style';

const ProjectManagerJob = () => (
	<div class="container">
		<h1>Project Manager</h1>
		<div class={style.pin} />
		<div class={style.pulse} />
		<p class={style.location}>Remote working opportunity</p>
		<p>The Video Bagel team is looking for a project manager with inside sales experience.</p>
		<h2 class={style.headline}>Responsibilities</h2>
		<ul class={style.requirementList}>
			<li>Managing multiple projects from start to finish</li>
			<li>Pre-sale communication with potential customers</li>
			<li>Ensuring project and customer success</li>
			<li>Coming up with creative video plot ideas</li>
		</ul>
		<h2 class={style.headline}>What We Offer</h2>
		<ul  class={style.requirementList}>
			<li>Work whatever hours you like</li>
			<li>Work from anywhere you want</li>
		</ul>
		<h2 class={style.headline}>Qualifications</h2>
		<ul  class={style.requirementList}>
			<li>Ability to identify strengths and weaknesses in designs &amp; videos</li>
			<li>Familiar with software like Trello and CRM software</li>
			<li>Conversion rate optimization experience (a plus)</li>
		</ul>
		<p>If you want to apply for this job write a friendly email to jobs@videobagel.com.</p>
	</div>
);

export default ProjectManagerJob;
