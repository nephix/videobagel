import { h, Component } from 'preact';
import style from './style';

export default class Privacy extends Component {
	render() {
		return (
			<div class="container">
				<h1>Privacy</h1>
				<h2 class={style.headline}>Cookies</h2>
				<script
					id="CookieDeclaration"
					src="https://consent.cookiebot.com/ed1aaa06-7538-4047-b430-31957cf6baba/cd.js"
					type="text/javascript"
					async
				/>

				<h2 class={style.headline}>What we might collect about you</h2>
				<p>We might collect certain types of personal data about you:</p>
				<ul class={style.list}>
					<li>
						<em>Identity Data</em>: In the event of a purchase, we collect your first name and your last name.
					</li>
					<li>
						<em>Contact Data</em>: In the event of a purchase, we collect your email address, your phone number and your billing address.
					</li>
					<li>
						<em>Financial Data</em>: In the event of a purchase via credit card, we store your masked credit card number (last 4 digits) and
						your card's expiration month and year.
					</li>
					<li>
						<em>Transaction Data</em> may include details about payments between us and other details of purchases made by you.
					</li>
					<li>
						<em>Technical Data</em> may include your login data, browser type and version, browser plug-in types and versions, time zone
						setting and location, operating system and platform and other technology on the devices you use to access this site.
					</li>
					<li>
						<em>Profile Data</em> may include your username and password, purchases or orders, your interests, preferences, feedback and
						survey responses.
					</li>
					<li>
						<em>Usage Data</em> may include information about how you use our website, products and services.
					</li>
					<li>
						<em>Marketing &amp; Communication Data</em> may include your preferences in receiving marketing communications from us and our
						third parties and your communication preferences.
					</li>
				</ul>
				<p>
					We may also process Aggregated Data from your personal data, but this data does not reveal your identity and as such in itself is
					not personal data. An example of this is where we review your Usage Data to work out the percentage of website users using a
					specific feature of our site.
				</p>
				<h3 class={style.headline}>Sensitive Data</h3>
				<p>
					Sensitive Data We do not collect any Sensitive Data about you. Sensitive data refers to data that includes details about your race
					or ethnicity, religious or philosophical beliefs, sex life, sexual orientation, political opinions, trade union membership,
					information about your health and genetic and biometric data. We do not collect any information about criminal convictions and
					offences.
				</p>

				<h2 class={style.headline}>How we collect your personal data</h2>
				<p>We collect data about you through a variety of different methods.</p>
				<h3 class={style.headline}>Direct Interactions:</h3>
				<p>
					You may provide data by filling in forms on our site (or otherwise) or by communicating with us by post, phone, email or
					otherwise, including when you:
				</p>
				<ul>
					<li>order our products or services</li>
					<li>create an account on our site</li>
					<li>subscribe to our service or publications</li>
					<li>request resources or marketing be sent to you</li>
					<li>enter a competition, prize draw, promotion or survey; or</li>
					<li>give us feedback.</li>
				</ul>
				<h3 class={style.headline}>Automated Technologies or Interactions</h3>
				<p>
					As you use our site, we may automatically collect Technical Data about your equipment, browsing actions and usage patterns. We
					collect this data by using cookies, server logs and similar technologies. We may also receive Technical Data about you if you
					visit other websites that use our cookies.
				</p>

				<h2 class={style.headline}>How we use your personal data</h2>
				<p>We will only use your personal data when legally permitted. The most common uses of your personal data are:</p>
				<ul>
					<li>Where we need to perform the contract between us, whether a paid contract or digital delivery of a free product.</li>
					<li>To give you access to our project management platform in order to complete a project you've ordered from us.</li>
					<li>
						Where it is necessary for our legitimate interests (or those of a third party) and your interests and fundamental rights do not
						override those interests.
					</li>
				</ul>
				<h3 class={style.headline}>Purposes for processing your personal data</h3>
				<p>
					Set out below is a description of the ways we intend to use your personal data and the legal grounds on which we will process such
					data. We have also explained what our legitimate interests are where relevant.
				</p>
				<div class={style.table}>
					<div class={style.header}>Purpose / Activity</div>
					<div class={style.header}>Type of Data</div>
					<div class={style.header}>Lawful basis for processing</div>

					<div>To register you as a new customer</div>
					<div>
						<ol>
							<li>Identity</li>
							<li>Contact</li>
						</ol>
					</div>
					<div>Performance of a contract with you</div>

					<div>
						To process and deliver your order including:
						<ol>
							<li>Manage payments, fees and charges</li>
							<li>Collect and recover money owed to us</li>
							<li>To give you access to our membership site, your courses and our forum</li>
						</ol>
					</div>
					<div>
						<ol>
							<li>Identity</li>
							<li>Contact</li>
							<li>Profile</li>
							<li>Usage</li>
							<li>Marketing and Communications</li>
							<li>Technical</li>
						</ol>
					</div>
					<div>
						Necessary for our legitimate interests to study how customers use our products/services, to develop them, to grow our business
						and to inform our marketing strategy
					</div>

					<div>To use data analytics to improve our website, products/services, marketing, customer relationships and experiences</div>
					<div>
						<ol>
							<li>Usage</li>
							<li>Technical</li>
						</ol>
					</div>
					<div>
						Necessary for our legitimate interests to define types of customers for our products and services, to keep our site updated and
						relevant, to develop our business and to inform our marketing strategy
					</div>

					<div>To make suggestions and recommendations to you about goods or services that may be of interest to you</div>
					<div>
						<ol>
							<li>Identity</li>
							<li>Contact</li>
							<li>Profile</li>
							<li>Usage</li>
							<li>Technical</li>
						</ol>
					</div>
					<div>Necessary for our legitimate interests to develop our products/services and grow our business</div>
				</div>

				<h3 class={style.headline}>Marketing communications</h3>
				<p>You will receive marketing communications from us if you have:</p>
				<ul class={style.list}>
					<li>requested information from us or purchased goods or services from us; or</li>
					<li>
						if you provided us with your details and ticked the box at the point of entry of your details for us to send you marketing
						communications or clicked a GDPR consent link in an email; and
					</li>
					<li>in each case, you have not opted out of receiving that marketing.</li>
				</ul>
				<p>We will get your express opt-in consent before we share your personal data with any third party for marketing purposes.</p>
				<p>
					You can ask us to stop sending you marketing messages at any time by clicking the ‘unsubscribe’ link at the bottom of newsletter
					emails.
				</p>
				<p>
					Where you opt out of receiving our marketing communications, this will not apply to personal data provided to us as a result of a
					product/service purchase, warranty registration, product/service experience or other transactions or our legal obligations for
					complying with business law.
				</p>

				<h2 class={style.headline}>Google Analytics</h2>
				<p>
					This website uses Google Analytics, a website analysis service by Google Inc., 1600 Amphitheatre Parkway Mountain View, CA 94043,
					USA.
				</p>
				<p>
					Google Analytics uses so-called &quot;cookies&quot;, text files that are stored on your computer to analyse your use of the
					website. The information generated by the cookie about your use of this website is usually sent to and stored at a Google server
					in the U.S.A.
				</p>
				<p>
					You will find more information concerning the treatment of user data by Google Analytics in Google's Privacy Policy:{' '}
					<a href="https://support.google.com/analytics/answer/6004245?hl=de"> https://support.google.com/analytics/answer/6004245?hl=de</a>
				</p>
				<p>
					<strong>Browser Plugin</strong>
				</p>
				<p>
					You may prevent the installation of cookies by adjusting the settings of your browser; however, if you do so, you may be unable to
					use all features of this website. Further, you may prevent the collection of data generated by cookies (including your IP address)
					and related to the use of these websites, as well as the processing of such data by Google, by downloading and installing the
					plug- in under the following link:{' '}
					<a href="http://tools.google.com/dlpage/gaoptout?hl=en"> http://tools.google.com/dlpage/gaoptout?hl=en</a>
				</p>
				<p>
					<strong>Objection to Data Collection</strong>
				</p>
				<p>
					You may prevent the collection of data by Google Analytics by clicking the following link. In doing so, an Opt-Out-Cookie will be
					placed which prevents future data collection when visiting our website:
					<a href="javascript:gaOptout();">Disable Google Analytics</a>
				</p>
				<p>
					<strong>IP-anonymisation</strong>
				</p>
				<p>
					We use the function &quot;Activate IP anonymisation&quot;. Through this, your IP address will be shortened in advance within the
					member states of the European Union or other contractual states of the Treaty on the European Economic Area. Only in exceptional
					cases, your complete IP address will be transmitted to a server in the U.S.A. and shortened there. Commissioned by the operators
					of this website, Google will use this information to evaluate your use of the website, to compile reports on website activities
					for website operators and to provide other services related to website and internet activities. Google will not merge your IP
					address with any other data held by Google.
				</p>

				<h2 class={style.headline}>Google Analytics Remarketing</h2>
				<p>
					Our websites use the functions of Google Analytics Remarketing in connection with the cross-device functions of Google AdWords and
					Google DoubleClick. The provider of these technologies is Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043, USA.
				</p>
				<p>
					This function enables a linking of commercial target groups compiled by Google Analytics Remarketing with the cross-device
					functions of Google AdWords and Google DoubleClick. As such, customized, interest-targeted commercials adapted for display on a
					terminal device (e.g. cell phone) in accordance with your previous usage and browsing behaviour can be also displayed on another
					of your terminal devices (e.g. PC, tablet).
				</p>
				<p>
					If you have given the appropriate permission, Google will link your web and app browser history with your Google account for this
					purpose. In this way, the same customized commercials will be displayed on any terminal device on which you are logged in to your
					Google account.
				</p>
				<p>
					In order to support this function, Google Analytics collects Google-authenticated IDs of users, which are temporarily linked to
					our Google Analytics Data, to identify and to compile target groups for cross-device advertising.
				</p>
				<p>
					You can permanently object to cross- device remarketing/targeting by deactivating customised commercials in your Google account by
					using the link below: <a href="https://www.google.com/settings/ads/onweb/"> https://www.google.com/settings/ads/onweb/</a>
				</p>
				<p>
					You will find further information and data protection regulations in Google's Privacy Policy under:{' '}
					<a href="http://www.google.com/policies/technologies/ads/"> http://www.google.com/policies/technologies/ads/</a>
				</p>

				<h2 class={style.headline}>Facebook Pixel</h2>
				<p>
					In the realm of usage-based online advertising, we make use of the “Custom Audience pixel” of Facebook Inc., 1601 S. California
					Ave, Palo Alto, CA 94304, USA (“Facebook”) on our website. With its help, we can keep track of what users do after they see or
					click on a Facebook advertisement. This enables us to monitor the effectiveness of Facebook ads for purposes of statistics and
					market research. Data collected in this way is anonymous to us, which means we cannot see the personal data of individual users.
					However, this data is saved and processed by Facebook, which we will inform you of to the best of our knowledge. Facebook can
					connect this data with your Facebook account and use it for its own advertising purposes, in accordance with Facebook’s{' '}
					<a href="https://www.facebook.com/about/privacy/">Data Policy</a>. You can allow Facebook and its partners to place ads on and
					outside of Facebook. A cookie can also be saved on your device for these purposes.
				</p>
				<p>
					Only users who are over 13 years of age can give their consent to this. If you are younger, we advise you to ask your parents or
					legal guardians for advice.
				</p>
				<p>
					Please click <a href="https://www.facebook.com/settings/?tab=ads#_=_">here</a> if you would like to withdraw your consent.
				</p>

				<h2 class={style.headline}>Mixpanel</h2>
				<p>
					We use technologies from Mixpanel, Inc. ("Mixpanel") on our website to perform statistical evaluations. This allows us to improve
					and optimize product features and make their designs more appealing.
				</p>
				<p>
					For our website, Mixpanel uses Cookies to detect your browser type, operating system, language settings, the search criteria that
					you enter and your IP address. Mixpanel then sends this data to a Mixpanel server in the USA. Mixpanel anonymizes your IP address
					and only the anonymized IP address is sent to us, ensuring that your privacy remains protected. If you do not want to consent to
					such data collection, please see our instructions for disabling Cookies in the website-related section below. Furthermore, you can
					opt out of Mixpanel collecting and storing such data at any time by following the instructions in the link below:
				</p>
				<p>
					<a href="https://mixpanel.com/optout/">Opt out here</a>
				</p>
				<p>
					More information about Mixpanel and its privacy policy can be found <a href="https://mixpanel.com/privacy/">here</a>.
				</p>

				<h2 class={style.headline}>SSL Encryption</h2>
				<p>
					For security reasons, and to protect the transmission of confidential data, such as enquiries you send to us as the provider, this
					website uses an SSL encryption. You can recognise an encrypted connection by the change in the address line of the browser from
					&quot;http://&quot; to &quot;https://&quot; and by the lock- symbol in your browser address line.
				</p>
				<p>When the SSL encryption is activated, the data you have transmitted to us cannot be accessed by third parties.</p>

				<h2 class={style.headline}>Transmission of data on contract conclusion for services and digital contents</h2>
				<p>
					We transmit personal data to third parties only if it is necessary to fulfill the contract, e.g transmitting data to the
					commissioned credit institution in charge of the payment.
				</p>
				<p>
					When you signup to any of our services or buy any of our products the user data you submit will be stored by Drift, a marketing
					automation tool we use. You can check their Privacy Policy following this link:{' '}
					<a href="https://www.drift.com/privacy-policy/">https://www.drift.com/privacy-policy/</a>
				</p>
				<p>At Video Bagel we use Stripe and PayPal for processing your payements.</p>
				<p>Video Bagel does not store credit card details and instead relies on Stripe and PayPal for this.</p>
				<ul>
					<li>
						Credit card payments are handled by Stripe Payments Europe, Ltd (“Stripe”). The collection and processing by these providers of
						your credit or debit card details and other personal data are governed by their respective terms and conditions. See
						https://stripe.com/de/privacy for more information. Stripe may from time to time provide us with information regarding the
						credits and debits made to your card in order to enable us to reconcile our accounts.
					</li>
					<li>
						Payment by PayPal is handled by PayPal (Europe) S.à r.l. &amp; Cie, S.C.A., 5th floor, 22–24 Boulevard Royal, L-2449 Luxembourg.
						Additional information about Paypal’s privacy policy is available in{' '}
						<a href="https://www.paypal.com/cg/webapps/mpp/ua/privacy-full">Paypal’s Privacy Policy</a>.
					</li>
				</ul>
				<p>
					Video Bagel does not store your billing address details to generate invoices. Invoices are generated by{' '}
					<a href="https://quaderno.io/privacy/">Quaderno.io</a> (Recrea Systems, S.L.U.) NIF B35635648 Fernando Guanarteme 111 C.P. 35010
					Las Palmas (Spain).
				</p>
				<p>
					In order to perform our services you are invited to our project management platform that uses Basecamp. You can view Basecamp's
					privacy policy here:
					<a href="https://basecamp.com/about/policies/privacy">https://basecamp.com/about/policies/privacy</a>
				</p>
				<p>
					There is no further transmission of data to third parties unless you have given your explicit permission. There is no transmission
					of data to third parties for commercial purposes.
				</p>

				<h2 class={style.headline}>Encrypted payment transaction on this website</h2>
				<p>
					Payment transactions via common methods of payment (Visa/Mastercard, direct debit) take place exclusively via an encrypted SSL
					connection. You can recognize an encrypted connection by the change in the browser line from &quot;http://&quot; to
					&quot;https://&quot; and the lock-symbol in your browser line.
				</p>
				<p>In using encrypted communication, any bank details, which you submit to us, cannot be read by third parties.</p>

				<h2 class={style.headline}>Objection to commercial mails</h2>
				<p>
					Providers are obligated to publish contact information in the Legal Notice/Impressum. The use of such contact information by third
					parties for the purpose of distributing unsolicited advertisements or other commercial information is prohibited. The operators of
					this website reserve the right to take legal measures against senders of unsolicited commercial information, e.g. spam emails etc.
				</p>

				<h2 class={style.headline}>Your legal rights</h2>
				<p>
					Under certain circumstances, you have rights under data protection laws in relation to your personal data. These include the right
					to:
				</p>
				<ul class={style.list}>
					<li>Request access to your personal data.</li>
					<li>Request correction of your personal data.</li>
					<li>Request erasure of your personal data.</li>
					<li>Object to processing of your personal data.</li>
					<li>Request restriction of processing your personal data.</li>
					<li>Right to withdraw consent.</li>
				</ul>
			</div>
		);
	}
}
