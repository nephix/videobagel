import { h, Component } from 'preact';
import Chat from '../../components/chat';
import { Text } from 'preact-i18n';
import style from './style';

export default class Confirmation extends Component {
	componentDidMount() {
		// track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/d3moCKfwtnoQrumnjAM',
				transaction_id: ''
			});
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Lead');
		}
	}

	render() {
		return (
			<div class="container">
				<div class={style.wrapper}>
					<h1>
						<Text id="confirmation.thanks">Thanks for Getting in Touch!</Text>
					</h1>
					<div class={style.icon}>
						<Chat />
					</div>
					<p class={style.stepText}>
						<Text id="confirmation.contactYou">We will contact you within 1 business day.</Text>
					</p>
				</div>
			</div>
		);
	}
}
