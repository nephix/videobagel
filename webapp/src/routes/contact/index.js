import { h } from 'preact';
import { Text } from 'preact-i18n';

import ContactForm from '../../components/contactForm';
import style from './style';

const Contact = () => (
	<div class="container">
		<div class={style.wrapper}>
			<h1>
				<Text id="contact.headline">Fill out the contact form below</Text>
			</h1>
			<ContactForm />
		</div>
	</div>
);

export default Contact;
