import { h, Component } from 'preact';
import { Text, withText } from 'preact-i18n';

import Checkmark from '../../components/checkmark';

import style from './style';

@withText({
	emailPlaceholder: <Text id="orderForm.email.placeholder">Type your email</Text>,
	billingAddressPlaceholder: (
		<Text id="questionnaire.billingAddress.placeholder">Type your billing address</Text>
	),
	productLinkPlaceholder: <Text id="questionnaire.productLink.placeholder">Type your website</Text>,
	brandFontsPlaceholder: (
		<Text id="questionnaire.brandingGuidelines.placeholder">
			Type your font names or a link to a website where we can see your fonts
		</Text>
	),
	brandColorPlaceholder: (
		<Text id="questionnaire.brandColors.placeholder">Type your colors in RGB or HEX format</Text>
	),
	exampleVideoPlaceholder: (
		<Text id="questionnaire.positiveExample.placeholder">
			Paste links to videos on e.g. YouTube or Vimeo
		</Text>
	),
	storylinePlaceholder: (
		<Text id="questionnaire.storyline.placeholder">
			If you already have a story in mind, let us know. If not, we will come up with something
		</Text>
	),
	callToActionPlaceholder: (
		<Text id="questionnaire.callToAction.placeholder">
			This will be the call to action of your video. Can be things like click the button below this
			video, type in your email address etc.
		</Text>
	),
	idealCustomerPlaceholder: (
		<Text id="questionnaire.idealCustomer.placeholder">
			Include things like age, gender, beliefs, goals, websites they like or dislike, ...
		</Text>
	),
	howHelpPlaceholder: (
		<Text id="questionnaire.howHelp.placeholder">Type how your product helps</Text>
	),
	customerProblemPlaceholder: (
		<Text id="questionnaire.customerProblem.placeholder">
			Include things like pains, problems, how often they have that problem, whether they know about
			a solution already or not, ...
		</Text>
	),
	additionalFeaturesPlaceholder: (
		<Text id="questionnaire.additionalFeatures.placeholder">
			Type additional features of your offer, like waterproof, free shipping, ...
		</Text>
	),
	commentsPlaceholder: (
		<Text id="questionnaire.comments.placeholder">If there's anything else, let us know...</Text>
	),
	submitText: <Text id="questionnaire.submitText">Submit Questionnaire</Text>
})
export default class Questionnaire extends Component {
	render({
		emailPlaceholder,
		billingAddressPlaceholder,
		productLinkPlaceholder,
		brandFontsPlaceholder,
		brandColorPlaceholder,
		exampleVideoPlaceholder,
		storylinePlaceholder,
		callToActionPlaceholder,
		idealCustomerPlaceholder,
		howHelpPlaceholder,
		customerProblemPlaceholder,
		additionalFeaturesPlaceholder,
		commentsPlaceholder,
		submitText
	}) {
		return (
			<div class="container">
				<Checkmark color="#5bd8a6" />
				<h1>
					<Text id="questionnaire.headline">Thank You! Your order was successfully placed.</Text>
				</h1>
				<p>
					<Text id="questionnaire.hintFillOut">
						Please fill out the questionnaire below so we can learn more about your project. After
						you've answered the questionnaire we will get in touch with you to answer all your
						remaining questions.
					</Text>
				</p>
				<p>
					<Text id="questionnaire.hintAddOns">
						The questionnaire will also give you the option to add more services to your order. If
						you wish to do so, we will sent you an invoice after you've submitted the questionnaire.
					</Text>
				</p>
				<div class={style.questionnaireWrapper}>
					<form
						class="form"
						action="https://www.getdrip.com/forms/27596686/submissions"
						method="post"
						data-drip-embedded-form="27596686"
					>
						<h2 data-drip-attribute="headline">
							<Text id="questionnaire.title">Questionnaire</Text>
						</h2>
						<div>
							<label for="drip-email">
								<Text id="orderForm.email.label">Email Address*</Text>
							</label>
							<input
								placeholder={emailPlaceholder}
								type="email"
								id="drip-email"
								name="fields[email]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-type">
								<Text id="questionnaire.videoType.label">
									What type of video are you looking for?*
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="explainerVideo"
									type="radio"
									name="fields[video_type]"
									value="Explainer Video"
									required
								/>
								<label for="explainerVideo">
									<Text id="questionnaire.videoType.explainer">Explainer Video</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="videoContent"
									type="radio"
									name="fields[video_type]"
									value="Video Content"
									required
								/>
								<label for="videoContent">
									<Text id="questionnaire.videoType.content">Video Content</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="salesVideo"
									type="radio"
									name="fields[video_type]"
									value="Sales Video"
									required
								/>
								<label for="salesVideo">
									<Text id="questionnaire.videoType.sales">Sales Video</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="commercial"
									type="radio"
									name="fields[video_type]"
									value="Video Advertisement"
								/>
								<label for="commercial">
									<Text id="questionnaire.videoType.ad">Video Advertisement</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="screenRecording"
									type="radio"
									name="fields[video_type]"
									value="Screen Recording"
								/>
								<label for="screenRecording">
									<Text id="questionnaire.videoType.screen">Web/Mobile App Screen Recording</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="whiteboard"
									type="radio"
									name="fields[video_type]"
									value="Whiteboard Video"
								/>
								<label for="whiteboard">
									<Text id="questionnaire.videoType.whiteboard">Whiteboard Video</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="videoTypeDontKnow"
									type="radio"
									name="fields[video_type]"
									value="I don't know"
								/>
								<label for="videoTypeDontKnow">
									<Text id="general.iDontKnow">I don't know</Text>
								</label>
							</div>
						</div>
						<div>
							<label for="drip-video-type">
								<Text id="questionnaire.videoGoal.label">
									The goal of the video is to get people to...*
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="clickVideo"
									type="radio"
									name="fields[video_goal]"
									value="Get people to my website"
									required
								/>
								<label for="clickVideo">
									<Text id="questionnaire.videoGoal.websiteClicks">Go to my website</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="purchaseVideo"
									type="radio"
									name="fields[video_goal]"
									value="Purchase my product"
								/>
								<label for="purchaseVideo">
									<Text id="questionnaire.videoGoal.purchase">Purchase my product</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="serviceVideo"
									type="radio"
									name="fields[video_goal]"
									value="Enquire for my service"
								/>
								<label for="serviceVideo">
									<Text id="questionnaire.videoGoal.enquire">Enquire for my service</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="teachVideo"
									type="radio"
									name="fields[video_goal]"
									value="Teach how it works"
								/>
								<label for="teachVideo">
									<Text id="questionnaire.videoGoal.teach">Teach how my product works</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="trustVideo"
									type="radio"
									name="fields[video_goal]"
									value="Trust our company"
								/>
								<label for="trustVideo">
									<Text id="questionnaire.videoGoal.trust">Trust our company</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="somethingElseVideo"
									type="radio"
									name="fields[video_goal]"
									value="Please follow up with client to find out exactly what they want"
								/>
								<label for="somethingElseVideo">
									<Text id="general.other">other</Text>
								</label>
							</div>
						</div>
						<div>
							<label for="drip-video-spokesperson">
								<Text id="questionnaire.actor.label">
									Do you want us to hire an actor or actress for an additional $149 per 30 seconds
									of video ordered?*
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="spokespersonYes"
									type="radio"
									name="fields[video_spokesperson]"
									value="Yes"
									required
								/>
								<label for="spokespersonYes">
									<Text id="general.yes">Yes</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="spokespersonNo"
									type="radio"
									name="fields[video_spokesperson]"
									value="No"
								/>
								<label for="spokespersonNo">
									<Text id="general.no">No</Text>
								</label>
							</div>
						</div>
						<div>
							<label for="drip-product-link">
								<Text id="questionnaire.productLink.label">
									What is the link to your product's or service' website?*
								</Text>
							</label>
							<input
								placeholder={productLinkPlaceholder}
								type="text"
								id="drip-product-link"
								name="fields[product_link]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="expressYes">
								<Text id="questionnaire.jumpAheadOfQueue.label">
									Would you like to jump ahead of the project queue for an additinal $99? If you
									order this add on then there won't be any initial waiting times, we will start to
									work on your project immediately.
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="expressYes"
									type="radio"
									name="fields[express_delivery]"
									value="Yes"
									required
								/>
								<Text id="general.yes">Yes</Text>
							</div>
							<div class="radio-row">
								<input id="expressNo" type="radio" name="fields[express_delivery]" value="No" />
								<Text id="general.no">No</Text>
							</div>
						</div>
						<div>
							<label for="drip-brand-fonts">
								<Text id="questionnaire.brandingGuidelines.label">
									Do use specific fonts in your branding guidelines?*
								</Text>
							</label>
							<textarea
								placeholder={brandFontsPlaceholder}
								type="text"
								id="drip-brand-fonts"
								name="fields[brand_fonts]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-brand-colors">
								<Text id="questionnaire.brandColors.label">How about brand colors?*</Text>
							</label>
							<textarea
								placeholder={brandColorPlaceholder}
								type="text"
								id="drip-brand-colors"
								name="fields[brand_colors]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-style">
								<Text id="questionnaire.finalVideo.label">
									Which of these words describe the style of your final video best?*
								</Text>
							</label>
							<textarea
								placeholder="Corporate, Modern, Simple, Crazy, Cartoon, Outside the box, Rock 'n Roll, Organic, Western"
								type="text"
								id="drip-video-style"
								name="fields[video_style]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-examples-positive">
								<Text id="questionnaire.positiveExample.label">
									Can you post an example of a video that you like?*
								</Text>
							</label>
							<textarea
								placeholder={exampleVideoPlaceholder}
								type="text"
								id="drip-video-examples-positive"
								name="fields[video_examples_positive]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-examples-negative">
								<Text id="questionnaire.negativeExample.label">
									How about one that you don't like at all?*
								</Text>
							</label>
							<textarea
								placeholder={exampleVideoPlaceholder}
								type="text"
								id="drip-video-examples-negative"
								name="fields[video_examples_negative]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-storyline">
								<Text id="questionnaire.storyline.label">
									Is there a particular storyline we should follow in the video?*
								</Text>
							</label>
							<textarea
								placeholder={storylinePlaceholder}
								type="text"
								id="drip-video-storyline"
								name="fields[video_storyline]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-subtitles">
								<Text id="questionnaire.subtitles.label">
									Do you want to add subtitles for $49? 90% of videos on social media are watched on
									mute.*
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="subtitlesYes"
									type="radio"
									name="fields[video_subtitles]"
									value="Yes"
									required
								/>
								<Text id="general.yes">Yes</Text>
							</div>
							<div class="radio-row">
								<input id="subtitlesNo" type="radio" name="fields[video_subtitles]" value="No" />
								<Text id="general.no">No</Text>
							</div>
						</div>
						<div>
							<label for="drip-video-cta">
								<Text id="questionnaire.callToAction.label">
									What do you want the viewer of the video to do next?*
								</Text>
							</label>
							<textarea
								placeholder={callToActionPlaceholder}
								type="text"
								id="drip-video-cta"
								name="fields[video_cta]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-customer-ideal">
								<Text id="questionnaire.idealCustomer.label">Who is your ideal customer?*</Text>
							</label>
							<textarea
								placeholder={idealCustomerPlaceholder}
								type="text"
								id="drip-customer-ideal"
								name="fields[customer_ideal]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-product-help">
								<Text id="questionnaire.howHelp.label">
									How does your product or service help this customer?*
								</Text>
							</label>
							<textarea
								placeholder={howHelpPlaceholder}
								type="text"
								id="drip-product-help"
								name="fields[product_help]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-customer-problem">
								<Text id="questionnaire.customerProblem.label">
									What is the biggest problem that your customers have?*
								</Text>
							</label>
							<textarea
								placeholder={customerProblemPlaceholder}
								type="text"
								id="drip-customer-problem"
								name="fields[customer_problem]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-product-features">
								<Text id="questionnaire.additionalFeatures.label">
									What are additional features that your product or service provides?*
								</Text>
							</label>
							<textarea
								placeholder={additionalFeaturesPlaceholder}
								type="text"
								id="drip-product-features"
								name="fields[product_features]"
								value=""
								required
							/>
						</div>
						<div>
							<label for="drip-video-graphics">
								<Text id="questionnaire.customGraphics.label">
									Do you need any custom graphics for $69 per graphic? Choose yes if you want an
									animated version of your product, a very specific character or a very detailed and
									unique environment.*
								</Text>
							</label>
							<div class="radio-row">
								<input
									id="customGraphicsYes"
									type="radio"
									name="fields[video_graphics]"
									value="Yes"
									required
								/>
								<label for="customGraphicsYes">
									<Text id="general.yes">Yes</Text>
								</label>
							</div>
							<div class="radio-row">
								<input
									id="customGraphicsNo"
									type="radio"
									name="fields[video_graphics]"
									value="No"
								/>
								<label for="customGraphicsNo">
									<Text id="general.no">No</Text>
								</label>
							</div>
						</div>
						<div>
							<label for="drip-comments">
								<Text id="questionnaire.comments.label">Any further comments?</Text>
							</label>
							<textarea
								placeholder={commentsPlaceholder}
								type="text"
								id="drip-comments"
								name="fields[comments]"
								value=""
							/>
						</div>
						<div class={style.consent}>
							<input
								type="checkbox"
								name="fields[eu_consent]"
								id="drip-eu-consent"
								value="granted"
							/>
							<label for="drip-eu-consent">
								<Text id="general.euConsent">
									I consent to be contacted and to receive useful information as well as special
									offers by email
								</Text>
							</label>
							<input
								type="hidden"
								name="fields[eu_consent_message]"
								value="I consent to receive information about services and special offers by email"
							/>
						</div>
						<div class="button-wrapper">
							<input
								class="submit"
								type="submit"
								value={submitText}
								data-drip-attribute="sign-up-button"
							/>
						</div>
					</form>
				</div>
			</div>
		);
	}
}
