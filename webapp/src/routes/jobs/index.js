import { h } from 'preact';
import { Link } from 'preact-router/match';

import style from './style';

const Jobs = () => (
	<div class="container">
		<h1>We're hiring!</h1>
		<p>Learn more by following one of the open positions below:</p>
		<ul class={style.openPositions}>
			<li><Link href="/jobs/project-manager">Project manager</Link></li>
			<li><Link href="/jobs/copywriter">Copywriter</Link></li>
		</ul>
		<p>If none of those suit your skills, we'd like to <a href="mailto:jobs@videobagel.com">hear from you</a> nonetheless!</p>
	</div>
);

export default Jobs;