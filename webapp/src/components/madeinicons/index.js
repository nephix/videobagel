import { h } from 'preact';
import style from './style';

const MadeInIcons = () => (
	<div class={style.icons}>
		<img class={style.americanCopywriters} src="/assets/paris-trained-designers.svg" alt="Paris-Trained Designers"/>
		<img class={style.americanCopywriters} src="/assets/american-copywriters.svg" alt="American Copywriters"/>
		<img class={style.madeInEU} src="/assets/madeineu.svg" alt="Made in EU"/>
	</div>
);

export default MadeInIcons;
