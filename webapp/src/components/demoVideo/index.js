import { h, Component } from 'preact';
import LazyLoad from 'react-lazy-load';
import { Text } from 'preact-i18n';

import Dialog from '../dialog';
import style from './style';

export default class DemoVideo extends Component {
	onThumbClick(e) {
		e.preventDefault;
		const { description } = this.props;

		const message = `Watched Demo Video "${description}"`;

		// Track mixpanel click
		if (window.mixpanel) {
			mixpanel.track(message);
		}

		// Track google analytics
		if (window.ga && ga.create) {
			ga('gtm1.send', {
				hitType: 'event',
				eventCategory: message
			});
		}
		this.setState({ playVideo: true });
	}

	onDemoDialogClosed() {
		const { state } = this;
		this.setState({ ...state, playVideo: false });
	}

	constructor(props) {
		super(props);
		this.state = { playVideo: false };
	}

	render() {
		const { country, thumb, embed, square } = this.props;
		const { playVideo } = this.state;
		return (
			<div class={style.demoVideoWrapper}>
				{country ? <span class={style.country}>{country}</span> : ''}
				<span onClick={e => this.onThumbClick(e)}>
					<LazyLoad offset={200}>
						<img
							class={style.thumb}
							src={thumb}
							alt="Video Commercial, Explainer Video, Animated Video"
						/>
					</LazyLoad>
					<i class={style.iconPlay} />
				</span>
				<Dialog open={playVideo} onClose={() => this.onDemoDialogClosed()}>
					<div class={square ? style.videoWrapperSquare : style.videoWrapper}>
						<div class={square ? style.embedContainerSquare : style.embedContainer}>
							<iframe
								src={embed + '?autoplay=1&loop=1&quality=720p'}
								frameborder="0"
								allowfullscreen
							/>
						</div>
					</div>
					<a class={style.btn} onClick={() => this.onDemoDialogClosed()}>
						<Text id="general.close">Close</Text>
					</a>
				</Dialog>
			</div>
		);
	}
}
