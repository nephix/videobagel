import { h, Component } from 'preact';
import { Text, withText } from 'preact-i18n';

import style from './style';

@withText({
	emailPlaceholder: <Text id="orderForm.email.placeholder">Type your email</Text>,
	messagePlaceholder: (
		<Text id="contact.message.placeholder">
			Type your message, include your mobile phone number or Skype name if possible
		</Text>
	),
	submitText: <Text id="contact.submit">Submit Message</Text>
})
export default class ContactForm extends Component {
	submitForm(e) {
		const email = document.querySelectorAll('#contactForm #drip-email')[0];
		const message = document.querySelectorAll('#contactForm #drip-comments')[0];
		const isEmailValid = email.validity.valid;
		const isMessageValid = message.validity.valid;

		const isFormValid = isEmailValid && isMessageValid;

		if (!isFormValid) {
			e.preventDefault();
			return false;
		}

		// track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/d3moCKfwtnoQrumnjAM',
				transaction_id: ''
			});
		}

		// Track mixpanel conversion
		if (window.mixpanel) {
			mixpanel.track('Submitted Contact Form');
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Lead');
		}

		return true;
	}
	render({ emailPlaceholder, messagePlaceholder, submitText }) {
		return (
			<div class={style.formWrapper}>
				<form
					class="form"
					id="contactForm"
					action="https://www.getdrip.com/forms/18155395/submissions"
					method="post"
					data-drip-embedded-form="18155395"
					onSubmit={e => this.submitForm(e)}
				>
					<div data-drip-attribute="description" />
					<div>
						<label for="drip-email">
							<Text id="orderForm.email.label">Email Address*</Text>
						</label>
						<input
							type="email"
							id="drip-email"
							name="fields[email]"
							value=""
							placeholder={emailPlaceholder}
							required
						/>
					</div>
					<div>
						<label for="drip-comments">
							<Text id="contact.message.label">Message*</Text>
						</label>
						<textarea
							type="text"
							id="drip-comments"
							name="fields[comments]"
							placeholder={messagePlaceholder}
							value=""
							required
						/>
					</div>
					<div class={style.consent}>
						<input type="checkbox" name="fields[eu_consent]" id="drip-eu-consent" value="granted" />
						<label for="drip-eu-consent">
							<Text id="general.euConsent">
								I consent to be contacted and to receive useful information as well as special
								offers by email
							</Text>
						</label>
						<input
							type="hidden"
							name="fields[eu_consent_message]"
							value="I consent to receive information about services and special offers by email"
						/>
					</div>
					<div class={style.buttonWrapper}>
						<input
							class="submit"
							type="submit"
							value={submitText}
							data-drip-attribute="sign-up-button"
						/>
					</div>
				</form>
			</div>
		);
	}
}
