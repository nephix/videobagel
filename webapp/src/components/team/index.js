import { h } from 'preact';
import LazyLoad from 'react-lazy-load';
import { Text, MarkupText } from 'preact-i18n';

import VideoCompanies from '../videocompanies';
import Guarantee from '../guarantee';

import style from './style';

const Team = () => (
	<div class={style.team}>
		<div class={style.memberWrapper}>
			<div class={style.copy}>
				<h2>
					<MarkupText id="team.doesntmatter.headline">
						It <em>doesn't matter</em>...
					</MarkupText>
				</h2>
				<p>
					<MarkupText id="team.doesntmatter.text">
						how good your product or service is, if your customers don't understand{' '}
						<em>what it does for them</em>. Most businesses focus so much on themselves, their
						product and what they do. Customers don't care. They want a transformation that leads
						them to a specific result.
					</MarkupText>
				</p>
			</div>
			<div class={style.member}>
				<LazyLoad height={214} offset={100}>
					<img src="../../assets/team/amanda.png" alt="Amanda" />
				</LazyLoad>
				<span class={style.name}>Amanda</span>
				<span class={style.location}>
					<Text id="countries.us">United States</Text>
				</span>
				<span class={style.position}>
					<Text id="jobs.copywriter">Copywriter</Text>
				</span>
			</div>
		</div>

		<VideoCompanies />

		<div class={style.memberWrapper}>
			<div class={style.copy}>
				<h2>
					<MarkupText id="team.247.headline">
						Your 24/7 <em>sales</em> or <em>support agent</em>.
					</MarkupText>
				</h2>
				<p>
					<MarkupText id="team.247.text">
						The beauty about animated videos is that once they're produced, they can reach an
						indefinite amount of people, regardless of the time of day. Through video you can find
						and onboard new customers or help out customers{' '}
						<em>without having any sales or customer support person being present</em>.
					</MarkupText>
				</p>
			</div>
			<div class={style.member}>
				<LazyLoad height={214} offset={100}>
					<img src="../../assets/team/andres.jpg" alt="Andres" />
				</LazyLoad>
				<span class={style.name}>Andres</span>
				<span class={style.location}>
					<Text id="countries.ca">Canada</Text>
				</span>
				<span class={style.position}>Motion Graphics Designer</span>
			</div>
		</div>

		<div class={style.completed}>
			<h2>
				<em>
					<Text id="team.accomplishments.together">Together, we</Text>
				</em>
				:
			</h2>
			<div class="container">
				<div class={style.completedWrapper}>
					<div>
						<p class={style.completedSubject}>
							<Text id="team.accomplishments.explain.top">Helped to Explain</Text>
						</p>
						<LazyLoad height={100}>
							<img src="assets/brain.svg" alt="Projects Completed" />
						</LazyLoad>
						<p class={style.completedAction}>
							<Text id="team.accomplishments.explain.bottom">
								70+ Different Products and Services.
							</Text>
						</p>
					</div>
					<div>
						<p class={style.completedSubject}>
							<Text id="team.accomplishments.adSpent.top">Helped 40+ Companies</Text>
						</p>
						<LazyLoad height={100}>
							<img src="assets/decrease.svg" alt="Projects Completed" />
						</LazyLoad>
						<p class={style.completedAction}>
							<Text id="team.accomplishments.adSpent.bottom">
								to Increase Their Conversions and Reduce Their Ad Spend.
							</Text>
						</p>
					</div>
					<div>
						<p class={style.completedSubject}>
							<Text id="team.accomplishments.created.top">Created 120+</Text>
						</p>
						<LazyLoad height={100}>
							<img src="assets/video-player.svg" alt="Projects Completed" />
						</LazyLoad>
						<p class={style.completedAction}>
							<Text id="team.accomplishments.created.bottom">
								Explainer Videos and Video Commercials.
							</Text>
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class={style.memberWrapper}>
			<div class={style.copy}>
				<h2>
					<MarkupText id="team.onepoint.headline">
						Address <em>one point</em> per video.
					</MarkupText>
				</h2>
				<p>
					<MarkupText id="team.onepoint.text">
						In order for your video investment to be profitable, the video needs to focus on a
						single, specific outcome. The <em>worst performing videos</em> try to tackle too many
						things at once, which leads the viewer to check out mentally.
					</MarkupText>
				</p>
			</div>
			<div class={style.member}>
				<LazyLoad height={214} offset={100}>
					<img src="../../assets/team/thomas.png" alt="Thomas" />
				</LazyLoad>
				<span class={style.name}>Thomas</span>
				<span class={style.location}>
					<Text id="countries.de">Germany</Text>
				</span>
				<span class={style.position}>
					<Text id="jobs.accountManager">Account Manager</Text>
				</span>
			</div>
		</div>

		<div class={style.completed}>
			<Guarantee />
		</div>

		<div class={style.memberWrapper}>
			<div class={style.copy}>
				<h2>
					<MarkupText id="team.message.headline">
						Your message has to be <em>on point</em>.
					</MarkupText>
				</h2>
				<p>
					<MarkupText id="team.message.text">
						If your message doesn't resonate with the viewer at all, no moving image can make the
						video work. The script is always the foundation &mdash; in the video we show{' '}
						<em>what isn't in the script</em>.
					</MarkupText>
				</p>
			</div>
			<div class={style.member}>
				<LazyLoad height={214} offset={100}>
					<img src="../../assets/team/branko.png" alt="Branko" />
				</LazyLoad>
				<span class={style.name}>Branko</span>
				<span class={style.location}>
					<Text id="countries.dm">Denmark</Text>
				</span>
				<span class={style.position}>Motion Graphics Designer</span>
			</div>
		</div>
	</div>
);

export default Team;
