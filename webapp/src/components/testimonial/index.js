import { h } from 'preact';
import LazyLoad from 'react-lazy-load';

import style from './style';

const Testimonial = ({ text, photo, date, name }) => (
	<div class={style.testimonialWrapper}>
		<div class={style.testimonial}>
			<div class={style.authorWrapper}>
				<span class={style.imageWrapper}>
					<LazyLoad height={40}>
						<img src={photo} alt={name} />
					</LazyLoad>
				</span>
				<div class={style.details}>
					<div class={style.firstAuthorLine}>
						<span class={style.name}>{name}</span>
						<span class={style.separator}> reviewed </span>
						<span href="https://www.facebook.com/videobagel" target="_blank" rel="noopener noreferrer" class={style.company}>
							Video Bagel
						</span>
						<span class={style.separator}> &mdash; </span>
						<span class={style.rating}>5 &#9733;</span>
					</div>
					<div class={style.secondAuthorLine}>
						<p class={style.date}>{date}</p>
					</div>
				</div>
			</div>
			<p class={style.testimonialText}>{text}</p>
		</div>
	</div>
);

export default Testimonial;
