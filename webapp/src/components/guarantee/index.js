import { h } from 'preact';
import { Text, MarkupText } from 'preact-i18n';

import style from './style';

const Guarantee = () => (
	<div class={style.guarantee}>
		<img src="/assets/guarantee.svg" alt="Guarantee" class={style.privacyImage} />
		<div class={style.guaranteeCopy}>
			<h3 class={style.guaranteeHeadline}>
				<Text id="guarantee.headline">Don't like the video? No Problem.</Text>
			</h3>
			<p>
				<MarkupText id="guarantee.text">
					Our <em>100% satisfaction guarantee</em> makes it a no brainer for you to work with us. If you don't like the video or if you feel
					that we've wasted your time you can ask for a full refund &mdash; without any hassle from us.
				</MarkupText>
			</p>
		</div>
	</div>
);

export default Guarantee;
