import { h } from 'preact';
import style from './style';

const Checkmark = ({color}) => (
	<div class={style.checkmarkWrapper}>
		<svg xmlns="http://www.w3.org/2000/svg" style="isolation:isolate" viewBox="-17.546 -6.862 170.292 148.525" width="170.292" height="148.525"><path class={style.pathCircle} d=" M 5.995 67.401 C 5.995 33.18 33.779 5.396 68 5.396 C 102.221 5.396 130.005 33.18 130.005 67.401 C 130.005 101.622 102.221 129.406 68 129.406 C 33.779 129.406 5.995 101.622 5.995 67.401 Z " fill="none" vector-effect="non-scaling-stroke" stroke-width="10" stroke={color} stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10"/><path class={style.pathCheck} d=" M 102.746 43.138 L 54.121 91.664 L 32.454 70.396" fill="none" vector-effect="non-scaling-stroke" stroke-width="10" stroke={color} stroke-linejoin="miter" stroke-linecap="round" stroke-miterlimit="10"/></svg>
	</div>
);

export default Checkmark;