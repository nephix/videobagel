import { h, Component } from 'preact';
import { Text, withText } from 'preact-i18n';

import style from './style';

@withText({
	buttonText: <Text id="orderForm.button">Submit My Order</Text>,
	firstNamePlaceholder: <Text id="orderForm.firstName.placeholder">Type your first name</Text>,
	lastNamePlaceholder: <Text id="orderForm.lastName.placeholder">Type your last name</Text>,
	emailPlaceholder: <Text id="orderForm.email.placeholder">Type your email</Text>,
	mobilePlaceholder: <Text id="orderForm.mobile.placeholder">+1234567890</Text>,
	projectDescriptionPlaceholder: (
		<Text id="orderForm.projectDescription.placeholder">
			Tell us in a couple of lines what your project is about
		</Text>
	)
})
export default class OrderForm extends Component {
	submitForm(e) {
		const firstName = document.getElementById('drip-first-name');
		const lastName = document.getElementById('drip-last-name');
		const email = document.getElementById('drip-email');
		const phone = document.getElementById('drip-mobile-phone');
		const description = document.getElementById('drip-project-description');
		const quantity = document.getElementById('drip-video-quantity');
		const length = document.querySelector('[name="fields[video_length]"]');
		const language = document.querySelector('[name="fields[video_language]"]');
		const isFirstNameValid = firstName.validity.valid;
		const isLastNameValid = lastName.validity.valid;
		const isEmailValid = email.validity.valid;
		const isPhoneValid = phone.validity.valid;
		const isDescriptionValid = description.validity.valid;
		const isQuantityValid = quantity.validity.valid;
		const isLengthValid = length.validity.valid;
		const isLanguageValid = language.validity.valid;

		const isFormValid =
			isFirstNameValid &&
			isLastNameValid &&
			isEmailValid &&
			isPhoneValid &&
			isDescriptionValid &&
			isQuantityValid &&
			isLengthValid &&
			isLanguageValid;

		if (!isFormValid) {
			e.preventDefault();
			return false;
		}

		// track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/Wfv7CLzlpHgQrumnjAM',
				currency: 'EUR',
				transaction_id: ''
			});
		}

		// Drip
		if (window._dcq) {
			_dcq.push(['track', 'Order Form']);
		}

		// Track mixpanel conversion
		if (window.mixpanel) {
			mixpanel.track('Purchased Video');
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Purchase');
		}

		return true;
	}

	render({
		buttonText,
		firstNamePlaceholder,
		lastNamePlaceholder,
		emailPlaceholder,
		mobilePlaceholder,
		projectDescriptionPlaceholder
	}) {
		return (
			<div class={style.orderForm}>
				<form
					action="https://www.getdrip.com/forms/701799214/submissions"
					method="post"
					data-drip-embedded-form="701799214"
					class="form"
					onSubmit={e => this.submitForm(e)}
					id="orderForm"
				>
					<div>
						<label for="drip-first-name">
							<Text id="orderForm.firstName.label">First name*</Text>
						</label>
						<input
							placeholder={firstNamePlaceholder}
							type="text"
							id="drip-first-name"
							name="fields[first_name]"
							value=""
							required
						/>
					</div>
					<div>
						<label for="drip-last-name">
							<Text id="orderForm.lastName.label">Last name*</Text>
						</label>
						<input
							placeholder={lastNamePlaceholder}
							type="text"
							id="drip-last-name"
							name="fields[last_name]"
							value=""
							required
						/>
					</div>
					<div>
						<label for="drip-email">
							<Text id="orderForm.email.label">Email Address*</Text>
						</label>
						<input
							placeholder={emailPlaceholder}
							type="email"
							id="drip-email"
							name="fields[email]"
							value=""
							required
						/>
					</div>
					<div>
						<label for="drip-mobile-phone">
							<Text id="orderForm.mobile.label">Mobile phone or Skype*</Text>
						</label>
						<input
							placeholder={mobilePlaceholder}
							type="text"
							id="drip-mobile-phone"
							name="fields[mobile_phone]"
							value=""
							required
						/>
					</div>
					<div>
						<label for="drip-project-description">
							<Text id="orderForm.projectDescription.label">Briefly Describe Your Project*</Text>
						</label>
						<textarea
							placeholder={projectDescriptionPlaceholder}
							type="text"
							id="drip-project-description"
							name="fields[project_description]"
							value=""
							required
						/>
					</div>
					<div>
						<label for="drip-video-quantity">
							<Text id="orderForm.quantity">How Many Videos Do You Want To Order?*</Text>
						</label>
						<select name="fields[video_quantity]" id="drip-video-quantity" required>
							<option value="1" selected>
								1
							</option>
							<option value="2">2</option>
							<option value="3">
								3 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="4">
								4 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="5">
								5 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="6">
								6 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="7">
								7 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="8">
								8 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="9">
								9 (10% <Text id="orderForm.discount">discount</Text>)
							</option>
							<option value="10+">
								10+ (20% <Text id="orderForm.discount">discount</Text>)
							</option>
						</select>
					</div>
					<div>
						<label for="drip-video-length">
							<Text id="orderForm.length">What's the Average Length per Video?*</Text>
						</label>
						<select name="fields[video_length]" id="drip-video-length" required>
							<option value="30" selected>
								30 <Text id="general.seconds">Seconds</Text> (699 €)
							</option>
							<option value="60">
								60 <Text id="general.seconds">Seconds</Text> (1.299 €)
							</option>
							<option value="90">
								1.5 <Text id="general.minutes">Minutes</Text> (1.899 €)
							</option>
							<option value="120">
								2 <Text id="general.minutes">Minutes</Text> (2.499 €)
							</option>
							<option value="150">
								2.5 <Text id="general.minutes">Minutes</Text> (3.099 €)
							</option>
							<option value="180">
								3 <Text id="general.minutes">Minutes</Text> (3.699 €)
							</option>
							<option value="210">
								3.5 <Text id="general.minutes">Minutes</Text> (4.299 €)
							</option>
							<option value="240">
								4 <Text id="general.minutes">Minutes</Text> (4.899 €)
							</option>
						</select>
					</div>
					<div>
						<label for="drip-video-language">
							<Text id="orderForm.language">Which Language Would You Like?*</Text>
						</label>
						<div class={style.languageWrapper}>
							<div class="radio-row">
								<input
									id="english"
									type="radio"
									name="fields[video_language]"
									value="English"
									required
								/>
								<label for="english">English</label>
							</div>
							<div class="radio-row">
								<input id="german" type="radio" name="fields[video_language]" value="German" />
								<label for="german">Deutsch</label>
							</div>
							<div class="radio-row">
								<input id="french" type="radio" name="fields[video_language]" value="French" />
								<label for="french">Français</label>
							</div>
							<div class="radio-row">
								<input id="norsk" type="radio" name="fields[video_language]" value="Norwegian" />
								<label for="norsk">Norsk</label>
							</div>
							<div class="radio-row">
								<input id="nederlands" type="radio" name="fields[video_language]" value="Dutch" />
								<label for="nederlands">Nederlands</label>
							</div>
							<div class="radio-row">
								<input id="svenska" type="radio" name="fields[video_language]" value="Svenska" />
								<label for="svenska">Svenska</label>
							</div>
							<div class="radio-row">
								<input id="espanol" type="radio" name="fields[video_language]" value="Spanish" />
								<label for="espanol">Español</label>
							</div>
							<div class="radio-row">
								<input id="italian" type="radio" name="fields[video_language]" value="Italian" />
								<label for="italian">Italiano</label>
							</div>
							<div class="radio-row">
								<input
									id="portuguese"
									type="radio"
									name="fields[video_language]"
									value="Portuguese"
								/>
								<label for="portuguese">Português</label>
							</div>
							<div class="radio-row">
								<input id="other" type="radio" name="fields[video_language]" value="other" />
								<label for="other">
									<Text id="general.other">other</Text>
								</label>
							</div>
						</div>
					</div>

					<p class={style.hint}>
						<Text id="orderForm.hint">
							You will not pay immediately after submitting the order form. We will then contact you
							via email or give you a phone call in order to clarify your remaining questions and
							learn more about your project. You will then receive an invoice that you can pay via
							credit card or PayPal.
						</Text>
					</p>

					<div class={style.consent}>
						<input type="checkbox" name="fields[eu_consent]" id="drip-eu-consent" value="granted" />
						<label for="drip-eu-consent">
							<Text id="general.euConsent">
								I consent to be contacted and to receive useful information as well as special
								offers by email
							</Text>
						</label>
						<input
							type="hidden"
							name="fields[eu_consent_message]"
							value="I consent to receive information about services and special offers by email"
						/>
					</div>

					<div class="button-wrapper">
						<input
							class="submit highlight"
							type="submit"
							value={buttonText}
							data-drip-attribute="sign-up-button"
							required
						/>
					</div>
				</form>
			</div>
		);
	}
}
