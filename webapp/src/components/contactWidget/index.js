import { h, Component } from 'preact';

import ContactForm from '../contactForm';
import Dialog from '../dialog';
import Email from '../email';

import style from './style';

export default class ContactWidget extends Component {
	constructor(props) {
		super(props);
		this.state = { showContactForm: false };
	}
	dialogClosed() {
		this.setState({ showContactForm: false });
	}
	render() {
		const { showContactForm } = this.state;
		return (
			<div>
				<Dialog open={showContactForm} onClose={() => this.dialogClosed()}>
					<div class={style.formWrapper}>
						<ContactForm />
					</div>
				</Dialog>
				<span onClick={() => this.setState({ showContactForm: true })} class={style.contactWidget}>
					<Email color="#fff" /> Get in touch
				</span>
			</div>
		);
	}
}
