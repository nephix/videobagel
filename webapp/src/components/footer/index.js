import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import { Text, withText } from 'preact-i18n';

import style from './style';

@withText({
	email: <Text id="footer.emailAddress">bWFpbHRvOmhlbGxvQHZpZGVvYmFnZWwuY29t</Text>
})
export default class Footer extends Component {
	onEmailClicked = () => {
		// track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/d3moCKfwtnoQrumnjAM',
				transaction_id: ''
			});
		}

		// Track mixpanel conversion
		if (window.mixpanel) {
			mixpanel.track('Clicked Contact Email');
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Lead');
		}
	};

	constructor(props) {
		super(props);
		this.state = { email: '' };
	}

	componentDidMount() {
		const { email } = this.props;
		this.setState({ email: atob(email) });
	}

	render() {
		const { email } = this.state;
		return (
			<footer class={style.footer}>
				<div class="container">
					<div class={style.footerAddress}>
						<h3>Video Bagel</h3>
						<address>
							Straße der Republik 24
							<br /> 15890 Eisenhüttenstadt
							<br />
							<Text id="countries.de">Germany</Text>
						</address>
						<p>
							<a href={email} onClick={() => onEmailClicked()}>
								<Text id="footer.emailText">hello(at)videobagel.com</Text>
							</a>
						</p>
						<p>+49 3364 7705714</p>
					</div>

					<div class="bagel">
						<svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100">
							<path
								fill="#CFD8DC"
								d="M81.007,17.616C72.877,9.411,61.741,5,49.335,5C36.931,5,25.612,9.228,17.484,17.434C9.354,25.639,5,37.062,5,49.583  c0,12.521,4.449,23.849,12.578,32.055C25.707,89.843,36.931,95,49.335,95c12.406,0,23.783-5,31.913-13.205  C89.377,73.589,95,62.105,95,49.583C95,37.062,89.136,25.822,81.007,17.616z M83.74,39.037l-3.036,0.815l0.595-1.924l1.953,0.273  L83.74,39.037z M79.722,30.616l-2.069,2.903l-3.135-1.561l-0.36-1.685L79.722,30.616z M54.377,21.904l0.948,0.166l-1.354,2.862  l-1.065-1.705L54.377,21.904z M47.524,14.229l4.827,2.817l-3.137,1.664L47.101,15.9L47.524,14.229z M41.025,21.69l-1.777,4.125  l-4.264-1.076l-0.889-1.973L41.025,21.69z M24.154,23.591l1.947,4.448l-3.029-0.452l-0.056-3.049L24.154,23.591z M20.65,37.605  l-1.93,3l-3.205-1.409l-0.44-1.667L20.65,37.605z M13.25,61.384l2.503-2.304l2.625,2.082l0.009,1.649L13.25,61.384z M17.366,50.02  l3.943-2.88l2.291,0.646l-4.037,6.694L17.366,50.02z M21.882,77.451l-0.234-3.039l2.892-0.724l1.155,0.897L21.882,77.451z   M24.736,67.948l-1.795-0.901l4.387-4.894l1.12,4.04L24.736,67.948z M26.902,38.143l-0.519-1.947l1.793-0.826l0.856,0.442  L26.902,38.143z M29.386,82.511l0.11-1.985l0.785-0.561l1.055,2.988L29.386,82.511z M35.456,76.045l0.738-3.448l1.534-0.764  l1.021,5.533L35.456,76.045z M44.95,87.692l-0.833-4.556l4.252-1.558l1.885,1.169L44.95,87.692z M51.366,78.111l-0.352-1.985  l1.856-0.667l0.814,0.513L51.366,78.111z M62.482,85.062L60.05,82.53l0.222-1.712l5.127,2.211L62.482,85.062z M62.862,73.271  l1.065-1.674l0.957-0.097l-0.541,3.125L62.862,73.271z M63.003,62.684c-3.294,3.326-7.809,5.725-12.835,5.725  c-5.027,0-9.639-2.3-12.934-5.626c-3.295-3.325-5.303-7.979-5.303-13.054c0-5.074,2.071-9.666,5.366-12.991  c3.295-3.326,7.843-5.145,12.871-5.145c5.027,0,9.614,1.781,12.91,5.107c3.295,3.325,4.992,7.955,4.992,13.029  C68.069,54.804,66.298,59.358,63.003,62.684z M64.303,26.64l-1.775-1.219l5.364-4.555l0.607,4.456L64.303,26.64z M74.75,77.773  l-3.661-1.863l-0.406-1.982l6.523,0.459L74.75,77.773z M70.387,63.955l3.247-1.305l1.482,0.858l-3.979,3.942L70.387,63.955z   M74.405,47.14l-1.093-3.351l0.938-1.44l3.656,4.248L74.405,47.14z M84.292,50.921l0.999,4.604l-4.275,1.73l-1.955-1.126  L84.292,50.921z M79.822,65.581l0.33-1.963l0.843-0.466l0.713,3.09L79.822,65.581z"
							/>
						</svg>
					</div>

					<div>
						<ul class={style.socialLinks} itemscope itemtype="http://schema.org/Organization">
							<li>
								<a itemprop="sameAs" href="https://www.facebook.com/videobagel" rel="noopener noreferrer" target="_blank">
									<img src="assets/fb.svg" alt="Facebook" />
								</a>
							</li>
							<li>
								<a itemprop="sameAs" href="https://www.instagram.com/videobagel" rel="noopener noreferrer" target="_blank">
									<img src="assets/instagram.svg" alt="Instagram" />
								</a>
							</li>
							<li>
								<a itemprop="sameAs" href="https://www.linkedin.com/company/videobagel/" rel="noopener noreferrer" target="_blank">
									<img src="assets/linkedin.svg" alt="LinkedIn" />
								</a>
							</li>
						</ul>
						<ul class={style.footerLinks}>
							<li>
								<Link class={style.footerLink} href="/jobs">
									Jobs
								</Link>
							</li>
							<li>
								<Link class={style.footerLink} href="/legal">
									<Text id="footer.legal">Legal</Text>
								</Link>
							</li>
							<li>
								<Link class={style.footerLink} href="/privacy">
									<Text id="footer.privacy">Privacy</Text>
								</Link>
							</li>
						</ul>
					</div>

					<p class={style.copyright}>
						&copy;{' '}
						<a href="https://www.videobagel.com" target="_blank" rel="noopener noreferrer">
							Video Bagel
						</a>
						. <Text id="footer.copyright">All rights Reserved.</Text>
					</p>
				</div>
			</footer>
		);
	}
}
