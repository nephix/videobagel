import { h } from 'preact';
import style from './style';

const Spinner = () => (
	<div class={style.spinner} />
);

export default Spinner;
