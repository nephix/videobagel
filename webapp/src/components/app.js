import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { IntlProvider } from 'preact-i18n';
import localeDE from '../i18n/de.json';

import Header from './header';
import Footer from './footer';
import Home from '../routes/home';
import PleaseConfirm from '../routes/please-confirm';
import Confirmation from '../routes/confirmation';
import Questionnaire from '../routes/questionnaire';
import Jobs from '../routes/jobs';
import Contact from '../routes/contact';
import ProjectManagerJob from '../routes/projectManager';
import CopywriterJob from '../routes/copywriter';
import Privacy from '../routes/privacy';
import Legal from '../routes/legal';
import OrderConfirmation from '../routes/order-confirmation';
import Maintenance from '../routes/maintenance';

const trackedPages = ['/contact'];

const simpleHeaderPages = ['/confirmation', '/submit-questionnaire'];

export default class App extends Component {

	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		const { url } = e;
		this.currentUrl = url;

		// Track mixpanel route change
		if (typeof window !== 'undefined' && window.mixpanel && trackedPages.indexOf(url) >= 0) {
			window.mixpanel.track('Page Loaded', { 'Page Path': url });
		}

		let showSimpleHeader = false;
		let isHomepage = false;
		let { state } = this;

		if (simpleHeaderPages.indexOf(url) >= 0) {
			showSimpleHeader = true;
		}

		if (url === '/' || url.indexOf('/#') >= 0) {
			isHomepage = true;
		}

		this.setState({ ...state, showSimpleHeader, isHomepage });
	};

	constructor(props) {
		super(props);
		// Cancelling the add to home screen dialog
		if (typeof window !== 'undefined') {
			window.addEventListener('beforeinstallprompt', e => {
				e.preventDefault();
				return false;
			});
		}

		this.state = {
			showSimpleHeader: false,
			isHomepage: true
		};
	}

	render() {
		const { showSimpleHeader, isHomepage } = this.state;

		// We stick to the fallback texts if the browser language isn't German
		let locale = null;
		if (typeof window !== 'undefined') {
			if (window.navigator.language.toLowerCase().indexOf('de') >= 0) {
				// Switch to German!
				locale = localeDE;
			}
		}

		// Hide the navigation menu in the app
		let hideMobileTabBar = '';
		if (showSimpleHeader) {
			hideMobileTabBar = 'hide-mobile-tabs';
		}

		return (
			<IntlProvider definition={locale}>
				<div id="app" className={hideMobileTabBar}>
					<Header simpleHeader={showSimpleHeader} isHomepage={isHomepage} />
					<main>
						<Router onChange={this.handleRoute}>
							<Home path="/" default />
							<Contact path="/contact" />
							<Jobs path="/jobs" />
							<PleaseConfirm path="/please-confirm" />
							<Confirmation path="/confirmation" />
							<Questionnaire path="/submit-questionnaire" />
							<ProjectManagerJob path="/jobs/project-manager" />
							<CopywriterJob path="/jobs/copywriter" />
							<Privacy path="/privacy" />
							<Legal path="/legal" />
							<Maintenance path="/maintenance" />
							<OrderConfirmation path="/order-confirmation" />
						</Router>
					</main>
					<Footer />
				</div>
			</IntlProvider>
		);
	}
}
