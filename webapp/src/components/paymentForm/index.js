import { h, Component } from 'preact';
import { route } from 'preact-router';
import { Text } from 'preact-i18n';

import style from './style';

//const QUADERNO_API_KEY = 'pk_test_XxXP352zMAspSpQaETuH';
const QUADERNO_API_KEY = 'pk_live_BSxN5RpP4SFaJjWayKRj';

export default class PaymentForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkoutStarted: false,
			selectedPackage: {
				lengthInSec: 30,
				price: 299,
				itemCode: '30SECONDVIDEO'
			},
			packages: [
				{
					lengthInSec: 30,
					price: 299,
					itemCode: '30SECONDVIDEO'
				},
				{
					lengthInSec: 60,
					price: 499,
					itemCode: '60SECONDVIDEO'
				},
				{
					lengthInSec: 90,
					price: 599,
					itemCode: '90SECONDVIDEO'
				},
				{
					lengthInSec: 120,
					price: 699,
					itemCode: '120SECONDVIDEO'
				}
			]
		};
	}

	number_format(number, decimals, dec_point, thousands_sep) {
		let n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = typeof thousands_sep === 'undefined' ? ',' : thousands_sep,
			dec = typeof dec_point === 'undefined' ? '.' : dec_point,
			toFixedFix = function(n, prec) {
				// Fix for IE parseFloat(0.55).toFixed(0) = 0;
				let k = Math.pow(10, prec);
				return Math.round(n * k) / k;
			},
			s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	renderPackageSelectOptions() {
		const { packages } = this.state;
		let options = [];
		for (let i = 0; i < packages.length; i++) {
			let { lengthInSec, price } = packages[i];
			let displayName = lengthInSec + ' Second Video – $' + this.number_format(price);
			options.push(<option value={lengthInSec}>{displayName}</option>);
		}
		return options;
	}
	onPackageChange(e) {
		const { packages } = this.state;
		const selectedLength = e.target.value;

		for (let i = 0; i < packages.length; i++) {
			const { lengthInSec } = packages[i];
			if (selectedLength == lengthInSec) {
				this.setState({ selectedPackage: packages[i], ...packages });
				break;
			}
		}
	}

	onPayPalButtonClicked(e) {
		const { itemCode, price } = this.state.selectedPackage;
		// Track in mix panel
		if (window.mixpanel) {
			mixpanel.track('Clicked Order Now Button');
		}

		// Track facebook addtocart
		if (window.fbq) {
			fbq('track', 'AddToCart', { currency: 'USD', value: Number(price) });
		}

		// Track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/zubaCMj8y4ABEK7pp4wD'
			});
		}

		// Handle quaderno checkout
		const handler = QuadernoCheckout.configure({
			key: QUADERNO_API_KEY,
			locale: 'en',
			color: '#5bd8a6',
			callback
		});

		let callback = function(data) {
			// Redirect to PayPal
			let action_url = QuadernoCheckout.apiURL();
			let paypal_data = JSON.parse(data.paypal_data);
			paypal_data.transaction.redirect_url = 'https://www.videobagel.com/submit-questionnaire';
			let form = document.createElement('form');
			form.setAttribute('method', 'POST');
			form.setAttribute('action', action_url);
			form.setAttribute('enctype', 'application/json');
			let hiddenField = document.createElement('input');
			hiddenField.setAttribute('type', 'hidden');
			hiddenField.setAttribute('name', 'paypal_data');
			hiddenField.setAttribute('value', JSON.stringify(paypal_data));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
		};

		// Track mixpanel conversion
		if (window.mixpanel) {
			mixpanel.track('Purchased Video', { price });
		}

		// Track facebook purchase
		if (window.fbq) {
			fbq('track', 'Purchase', { currency: 'USD', value: Number(price) });
		}

		// Track adwords conversion
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/Wfv7CLzlpHgQrumnjAM',
				value: price.toString(),
				currency: 'USD',
				transaction_id: '',
				event_callback: callback
			});
		}

		const quadernoPrice = price + '00';

		handler.open({
			gateway: 'paypal',
			type: 'charge',
			taxes: 'included',
			item_code: itemCode,
			transaction_type: 'standard',
			currency: 'USD',
			billing_address: true,
			amount: quadernoPrice,
			callback
		});
		e.preventDefault();
	}

	onStripeButtonClicked(e) {
		const { itemCode, price } = this.state.selectedPackage;
		// Track in mix panel
		if (window.mixpanel) {
			mixpanel.track('Clicked Order Now Button');
		}

		// Track facebook addtocart
		if (window.fbq) {
			fbq('track', 'AddToCart', { currency: 'USD', value: Number(price) });
		}

		// Track adwords
		if (window.gtag) {
			gtag('event', 'conversion', {
				send_to: 'AW-831124654/zubaCMj8y4ABEK7pp4wD'
			});
		}

		// Handle quaderno checkout
		const handler = QuadernoCheckout.configure({
			key: QUADERNO_API_KEY,
			locale: 'en',
			color: '#5bd8a6',
			callback: data => {
				let callback = () => {
					route('/submit-questionnaire');
				};

				// Track facebook purchase
				if (window.fbq) {
					fbq('track', 'Purchase', { currency: 'USD', value: Number(price) });
				}

				// Track adwords conversion
				if (window.gtag) {
					gtag('event', 'conversion', {
						send_to: 'AW-831124654/Wfv7CLzlpHgQrumnjAM',
						value: price.toString(),
						currency: 'USD',
						transaction_id: '',
						event_callback: callback
					});
				}

				// Track mixpanel conversion
				if (window.mixpanel) {
					mixpanel.track('Purchased Video', { price });
				}
			}
		});

		const quadernoPrice = price + '00';

		handler.open({
			gateway: 'stripe',
			type: 'charge',
			taxes: 'included',
			item_code: itemCode,
			transaction_type: 'standard',
			currency: 'USD',
			billing_address: true,
			amount: quadernoPrice
		});

		e.preventDefault();
	}

	render() {
		return (
			<div class={style.orderForm}>
				<label for="package">
					<Text id="paymentForm.choosePackage">Select Your Package</Text>
				</label>
				<div class={style.selectPackageWrapper}>
					<select
						class={style.packageSelect}
						name="package"
						id="package"
						onChange={e => this.onPackageChange(e)}
					>
						{this.renderPackageSelectOptions()}
					</select>
				</div>
				<label>
					<Text id="paymentForm.startCheckout">Begin Checkout</Text>
				</label>
				<div class={style.orderBtnWrapper}>
					<button onClick={e => this.onStripeButtonClicked(e)} class={style.btn}>
						<img src="assets/credit-card.svg" alt="Credit card" />
						<span class={style.creditCard}>
							<Text id="paymentForm.creditCard">Credit Card</Text>
						</span>
					</button>
					<span class={style.separator}>or</span>
					<button onClick={e => this.onPayPalButtonClicked(e)} class={style.payPalBtn}>
						<img src="assets/paypal.svg" alt="PayPal" />
					</button>
				</div>
				<label>
					<Text id="paymentForm.filloutQuestionnaire">Fill Out Our Questionnaire</Text>
				</label>
				<p class={style.questionnaire}>
					<Text id="paymentForm.questionnaireHint">
						After your payment you will be redirected to a short questionnaire.
					</Text>
				</p>
			</div>
		);
	}
}
