import { h, Component } from 'preact';
import Portal from 'preact-portal';

import style from './style';

export default class Dialog extends Component {
	onCloseClicked(event) {
		event.preventDefault();

		// Call callback
		this.props.onClose();
	}
	render() {
		const { open, children } = this.props;
		return open ? (
			<Portal into="body">
				<div class={style.modal}>
					<div class={style.modalClose}>
						<a onClick={e => this.onCloseClicked(e)}>×</a>
					</div>
					<div class={style.modalBody}>{children}</div>
				</div>
			</Portal>
		) : null;
	}
}
