import { h } from 'preact';
import LazyLoad from 'react-lazy-load';
import { MarkupText } from 'preact-i18n';

import style from './style';

const VideoCompanies = () => (
	<div class="container">
		<div class={style.companies}>
			<h2 class={style.headline}>
				<MarkupText id="videoCompanies.headline">
					Companies that use <em>animated explainer videos in their marketing</em>
				</MarkupText>
			</h2>
			<div class={style.icons}>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/amazon.svg" alt="Amazon" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/google.svg" alt="Google" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img
						class={style.googleDevelopers}
						src="/assets/google-developers.svg"
						alt="Google Developers"
					/>
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/facebook_text.svg" alt="Facebook" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/ikea.svg" alt="IKEA" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/microsoft.svg" alt="Microsoft" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/cisco.svg" alt="cisco" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/wall-street-journal.svg" alt="WSJ" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/shopify.svg" alt="Shopify" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/stripe.svg" alt="Stripe" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/coca-cola.svg" alt="Coca Cola" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/vox.svg" alt="Vox" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/hubspot.svg" alt="HubSpot" />
				</LazyLoad>
				<LazyLoad height={40} offset={100}>
					<img src="/assets/bitcoin.svg" alt="Bitcoin" />
				</LazyLoad>
			</div>
		</div>
	</div>
);

export default VideoCompanies;
