import { h, Component } from 'preact';
import { Text, withText } from 'preact-i18n';
import InputRange from 'react-input-range';

import style from './style';

@withText({
	sliderHint: <Text id="priceRangeSlider.sliderHint">Use Slider for Pricing</Text>,
	mostPopular: <Text id="priceRangeSlider.mostPopular">most popular</Text>
})
export default class PriceRangeSlider extends Component {
	trackRangeSlider(e) {
		// Track in mixpanel
		if (window.mixpanel) {
			mixpanel.track('Used Range Slider', {
				sliderValue: e
			});
		}

		// Track google analytics
		if (window.ga && ga.create) {
			ga('gtm1.send', {
				hitType: 'event',
				eventCategory: 'Price Slider',
				eventAction: 'Changed',
				eventLabel: e
			});
		}
	}

	formatCurrency(number, decimals, dec_point, thousands_sep) {
		let n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = typeof thousands_sep === 'undefined' ? '.' : thousands_sep,
			dec = typeof dec_point === 'undefined' ? ',' : dec_point,
			toFixedFix = (n, prec) => {
				// Fix for IE parseFloat(0.55).toFixed(0) = 0;
				let k = Math.pow(10, prec);
				return Math.round(n * k) / k;
			},
			s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	handleChange = value => {
		let displayPrice = 0;
		let discount = 0;
		let salesPrice = 0;
		let showContactUs = this.state;
		let save = 0;

		if (value >= 0 && value <= 30.0) {
			displayPrice = 699.0;
			save = 1099;
		}
		if (value > 30.0 && value <= 60.0) {
			displayPrice = 1299.0;
			save = 2299;
		}
		if (value > 60.0 && value <= 90.0) {
			displayPrice = 1899.0;
			save = 3499;
		}
		if (value > 90.0 && value <= 120.0) {
			displayPrice = 2499.0;
			save = 4699;
		}
		if (value > 120.0 && value <= 150.0) {
			displayPrice = 3099.0;
			save = 5899;
		}
		if (value > 150.0 && value <= 180.0) {
			displayPrice = 3699.0;
			save = 7099;
		}
		if (value > 180.0 && value <= 210.0) {
			displayPrice = 4299.0;
			save = 8299;
		}
		if (value > 210.0 && value <= 240.0) {
			displayPrice = 4899.0;
			save = 9499;
		}
		if (value > 240.0) {
			showContactUs = true;
		}
		else {
			showContactUs = false;
		}
		let words = parseInt(2.5 * value, 10);
		salesPrice = displayPrice - displayPrice * discount;
		this.setState({ value, displayPrice, showContactUs, words, salesPrice, save });
	};

	constructor(props) {
		super(props);
		this.state = {
			value: 30,
			salesPrice: 699,
			save: 1099,
			words: 75,
			showContactUs: false
		};
	}

	render({ sliderHint, mostPopular }) {
		const { value, save, showContactUs, words, salesPrice } = this.state;
		let thousandsSeperator = ',';
		if (typeof window !== 'undefined') {
			const isGerman = window.navigator.language.toLowerCase().indexOf('de') >= 0;
			if (isGerman) {
				thousandsSeperator = '.';
			}
		}
		return (
			<div class={style.priceSliderWrapper}>
				<span class={style.useSliderHint} data-label={sliderHint}>
					<img src="assets/reply.svg" alt={sliderHint} />
				</span>
				{value <= 60.0 && value > 30 ? (
					<span class={style.mostPopular} data-label={mostPopular} />
				) : (
					''
				)}
				<p class={style.videoLength}>
					<span class={style.seconds}>{value > 60 ? ((value * 2) / 120.0).toFixed(1) : value}</span>{' '}
					{value > 60 ? (
						<Text id="general.minutes">Minute</Text>
					) : (
						<Text id="general.seconds">Second</Text>
					)}{' '}
					Video
				</p>
				{!showContactUs ? (
					<div class={style.priceWrapper}>
						<div class={style.price}>
							{this.formatCurrency(Math.floor(salesPrice), null, null, thousandsSeperator)}
							<sup> &euro;</sup>
						</div>
						{save ? (
							<div class={style.save}>
								{this.formatCurrency(Math.floor(save), null, null, thousandsSeperator)}
								<sup> &euro;</sup>
							</div>
						) : (
							''
						)}

						<div class={style.vat}>
							<Text id="priceRangeSlider.vat">incl. VAT</Text>
						</div>

						<div class={style.wordCount}>
							<span>
								<Text id="priceRangeSlider.scriptLength">Script</Text>
							</span>
							: <Text id="general.around">around</Text>{' '}
							<strong>
								{words} <Text id="general.words">words</Text>
							</strong>
						</div>
					</div>
				) : (
					<div class={style.askForQuote}>
						<Text id="general.contactUs">Get in touch with us</Text>
					</div>
				)}
				<InputRange
					minValue={5}
					maxValue={270}
					value={value}
					onChange={this.handleChange}
					onChangeComplete={e => this.trackRangeSlider(e)}
				/>
			</div>
		);
	}
}
