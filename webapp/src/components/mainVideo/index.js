import { h, Component } from 'preact';
import style from './style';

export default class MainVideo extends Component {
	onThumbClick(e) {
		e.preventDefault();
		const { alt } = this.props;
		this.setState({ playVideo: true });
		const message = `Watched Video "${alt}"`;
		// Track mixpanel video view
		if (window.mixpanel) {
			mixpanel.track(message);
		}

		// Track google analytics
		if (window.ga && ga.create) {
			ga('gtm1.send', {
				hitType: 'event',
				eventCategory: message
			});
		}
	}

	constructor(props) {
		super(props);
		this.state = {
			playVideo: false
		};
	}

	render() {
		const { vimeoUrl, thumb, alt } = this.props;
		const { playVideo } = this.state;
		const embedUrl = `${vimeoUrl}?autoplay=1&quality=720p`;
		return (
			<div class={style.videoWrapper}>
				<div class={style.embedContainer}>
					{!playVideo && (
						<a href="#" onClick={e => this.onThumbClick(e)}>
							<img class={style.thumb} src={thumb} alt={alt} />
							<i class={style.iconPlay} />
						</a>
					)}
					{playVideo && <iframe src={embedUrl} frameborder="0" allowFullScreen="" />}
				</div>
			</div>
		);
	}
}
