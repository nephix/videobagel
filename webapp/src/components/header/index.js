import { h, Component } from 'preact';
import { Text } from 'preact-i18n';
import { Link } from 'preact-router/match';

import style from './style';

export default class Header extends Component {
	componentDidUnmount() {
		const navScrollItems = document.querySelectorAll('.navLink__2ZmvR[href^="/#"]');
		for (let i = 0, len = navScrollItems.length; i < len; i++) {
			navScrollItems[i].addEventListener('click');
		}
	}

	onClickNavScrollItem(event) {
		event.preventDefault();

		const { tagName } = event.target;
		let href;

		if (tagName === 'IMG') {
			href = event.target.parentElement.parentElement.href;
		}
		else if (tagName === 'SPAN') {
			href = event.target.parentElement.href;
		}
		else {
			href = event.target.href;
		}

		if (href) {
			const section = href.split('/#')[1];
			const elem = document.getElementById(section);
			if (elem) {
				elem.scrollIntoView({ block: 'start', inline: 'nearest', behavior: 'smooth' });
			}
		}
	}

	constructor(props) {
		super(props);
		const { simpleHeader, isHomepage } = this.props;
		this.state = { simpleHeader, isHomepage };
	}

	componentDidMount() {
		const { simpleHeader, isHomepage } = this.props;
		if (!simpleHeader && isHomepage) {
			const navScrollItems = document.querySelectorAll('.navLink__2ZmvR[href^="/#"]');
			for (let i = 0, len = navScrollItems.length; i < len; i++) {
				navScrollItems[i].addEventListener('click', event => this.onClickNavScrollItem(event));
			}
		}
	}

	render() {
		const { simpleHeader, isHomepage } = this.props;

		if (simpleHeader) {
			return (
				<header class={style.headerSimple}>
					<Link class={style.brand}>
						<img class="bagel" src="../../assets/videobagel.svg" alt="Video Bagel" />
						Video Bagel
					</Link>
				</header>
			);
		}

		if (!isHomepage) {
			console.log('not on homepage');
		}

		return (
			<header class={style.header}>
				<Link href="/" class={style.brand}>
					<img class="bagel" src="../../assets/videobagel.svg" alt="Video Bagel" />
					Video Bagel
				</Link>
				<nav class={style.nav}>
					<a class={style.navLink} href="/#reviews">
						<span class={style.navIcon}>
							<img src="../../assets/star.svg" alt="Reviews" />
						</span>
						<Text id="navigation.reviews">Reviews</Text>
					</a>
					<a class={style.navLink} href="/#examples">
						<span class={style.navIcon}>
							<img src="../../assets/play.svg" alt="Examples" />
						</span>
						<Text id="navigation.examples">Examples</Text>
					</a>
					<a class={style.navLink} href="/#team">
						<span>Team</span>
					</a>
					<Link class={style.navLink} href="/contact">
						<span class={style.navIcon}>
							<img src="../../assets/email.svg" alt="Pricing" />
						</span>
						<Text id="navigation.contact">Contact</Text>
					</Link>
					<a class={style.navLink} href="/#pricing">
						<span class={style.navIcon}>
							<img src="../../assets/price.svg" alt="Pricing" />
						</span>
						<Text id="navigation.pricing">Pricing</Text>
					</a>
					<a class={style.navLink} href="/#order">
						<span class={style.navIcon}>
							<img src="../../assets/shopping-cart.svg" alt="Order Now" />
						</span>
						<Text id="navigation.orderNow">Order Now</Text>
					</a>
				</nav>
			</header>
		);
	}
}
