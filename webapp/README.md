This read me describes how to develop for the website https://www.videobagel.com locally as well as how to make production builds.

# How to start a local development server

To run a local development server you need to install preact-cli and other project dependencies:

```sh
$ npm install -g preact-cli
$ npm install
```

You'll be able to run the development server by running:

```sh
$ preact watch
```

or

```sh
$ npm start
```

You can also start a development server with the production build template which loads a bunch of third-party scripts through Google Tag Manager like this:

```sh
$ preact watch --template src/template.html
```

# How to make a production build

If you haven't installed `preact-cli` and other project dependencies, run:

```sh
$ npm install -g preact-cli
$ npm install
```

Then to start the build process, run:

```sh
$ preact build --clean --template src/template.html
```

Because of an unknown bug in `preact-cli` the project needs to be built with a specific version using the npx command:

```sh
$ npx preact-cli@2.1.3 build --clean --template src/template.html
```

Then upload the content of `/build` to the website.
